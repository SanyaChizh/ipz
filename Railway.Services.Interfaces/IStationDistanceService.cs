﻿using Railway.Services.Dto;
using Railway.Services.Interfaces.Validators;

namespace Railway.Services.Interfaces
{
    public interface IStationDistanceService : IDomainEntityService<StationsDistanceDto>
    {
        int Create(
            int firstStationId, 
            int secondStationId, 
            [RangeDouble] double distance);

        StationsDistanceDto FindStationDistance(int firstStationId, int secondStationId);
    }
}
