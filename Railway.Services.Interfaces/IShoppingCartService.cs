﻿using Railway.Services.Dto;

namespace Railway.Services.Interfaces
{
    public interface IShoppingCartService : IDomainEntityService<ShoppingCartDto>
    {
        int Create();

        void AddReservedTicketToShoppingCart(int shoppingCartId, int ticketId);

        void AddBoughtTicketToShoppingCart(int shoppingCartId, int ticketId);
    }
}
