﻿using Railway.Services.Dto;
using Railway.Services.Interfaces.Validators;

namespace Railway.Services.Interfaces
{
    public interface IClientService : IDomainEntityService<ClientDto>
    {
        int Create(
            [NonEmptyString] string firstName, 
            [NonEmptyString] string lastName, 
            [Email] string email);

        ClientDto GetClient(
            [NonEmptyString] string firstName,
            [NonEmptyString] string lastName, 
            [Email] string email);
    }
}
