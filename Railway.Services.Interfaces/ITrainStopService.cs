﻿using System;
using System.Collections.Generic;
using Railway.Services.Dto;
using Railway.Services.Interfaces.Utils;

namespace Railway.Services.Interfaces
{
    public interface ITrainStopService : IDomainEntityService<TrainStopDto>
    {
        int Create(int stationId, TimeSpan duration);

        IDictionary<string, TimeSpan> GetStopsForRoute(int routeId);

        IEnumerable<TrainStopInformation> GetStopsForFlight(int flightId);
    }
}
