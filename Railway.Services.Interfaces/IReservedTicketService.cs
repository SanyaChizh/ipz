﻿using Railway.Services.Dto;

namespace Railway.Services.Interfaces
{
    public interface IReservedTicketService : IDomainEntityService<TicketDto>
    {
        int ReserveTicket(int clientId, int placeId, int departurePointId, int arrivalPointId);
    }
}
