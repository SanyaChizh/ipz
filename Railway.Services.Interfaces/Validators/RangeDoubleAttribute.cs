﻿using System;
using Railway.Exceptions.Service;

namespace Railway.Services.Interfaces.Validators
{
    public class RangeDoubleAttribute : ArgumentValidationAttribute
    {
        public double MinValue { get; set; } = 0;

        public double MaxValue { get; set; } = double.MaxValue;

        public bool IncludingMin { get; set; } = true;

        public bool IncludingMax { get; set; } = true;

        public override void ValidateArgument(ArgumentExceptionDetails details)
        {
            try
            {
                CheckValue((double)details.Value);
            }
            catch (ArgumentException e)
            {
                details.Message = e.Message;
                throw new InvalidArgumentException(details);
            }
        }

        protected void CheckValue(double value)
        {
            if ((IncludingMin && value.CompareTo(MinValue) < 0) ||
                (!IncludingMin && value.CompareTo(MinValue) <= 0))
            {
                throw new ArgumentException("Minimal range violated");
            }

            if ((IncludingMax && value.CompareTo(MaxValue) > 0) ||
                (!IncludingMax && value.CompareTo(MaxValue) >= 0))
            {
                throw new ArgumentException("Maximal range violated");
            }
        }
    }
}
