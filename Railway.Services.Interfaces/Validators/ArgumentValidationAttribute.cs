﻿using System;
using Railway.Exceptions.Service;

namespace Railway.Services.Interfaces.Validators
{
    [AttributeUsage(AttributeTargets.Parameter)]
    public abstract class ArgumentValidationAttribute : Attribute
    {
        public abstract void ValidateArgument(ArgumentExceptionDetails details);
    }
}
