﻿using System.Text.RegularExpressions;
using Railway.Exceptions.Service;

namespace Railway.Services.Interfaces.Validators
{
    public class EmailAttribute : NonEmptyStringAttribute
    {
        public override void ValidateArgument(ArgumentExceptionDetails details)
        {
            base.ValidateArgument(details);
            const string emailRegex = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
            var value = (string)details.Value;
            if (Regex.IsMatch(value, emailRegex))
            {
                return;
            }

            details.Message = "Value is not email";
            throw new InvalidArgumentException(details);
        }
    }
}
