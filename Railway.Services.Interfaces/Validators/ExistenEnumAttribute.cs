﻿using System;
using Railway.Exceptions.Service;

namespace Railway.Services.Interfaces.Validators
{
    public class ExistenEnumAttribute : ArgumentValidationAttribute
    {
        public override void ValidateArgument(ArgumentExceptionDetails details)
        {
            var value = (Enum)details.Value;
            if (Enum.IsDefined(value.GetType(), value) == false)
            {
                details.Message = "Undefined value of enum";
                throw new InvalidArgumentException(details);
            }
        }
    }
}
