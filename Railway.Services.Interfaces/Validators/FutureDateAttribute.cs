﻿using System;
using Railway.Exceptions.Service;

namespace Railway.Services.Interfaces.Validators
{
    public class FutureDateAttribute : ArgumentValidationAttribute
    {
        public override void ValidateArgument(ArgumentExceptionDetails details)
        {
            var data = (DateTime)details.Value;
            if (data < DateTime.UtcNow)
            {
                details.Message = "Creating date(time) must be after current date(time)";
                throw new InvalidArgumentException(details);
            }
        }
    }
}
