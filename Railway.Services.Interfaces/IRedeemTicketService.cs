﻿using Railway.Services.Dto;
using Railway.Services.Interfaces.Validators;

namespace Railway.Services.Interfaces
{
    public interface IRedeemTicketService : IDomainEntityService<TicketDto>
    {
        int RedeemTicket(
            int reservedTicketId, 
            bool isExistsBaggage, 
            [RangeInt] int amountOfTea, 
            [RangeInt] int amountOfLinens);
    }
}
