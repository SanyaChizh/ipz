﻿using System;
using System.Collections.Generic;
using Railway.Services.Dto;
using Railway.Services.Interfaces.Validators;

namespace Railway.Services.Interfaces
{
    public interface IFlightService : IDomainEntityService<FlightDto>
    {
        int Create(
            [FutureDate] DateTime departureDate, 
            int trainId, 
            int routeId, 
            [RangeInt(MaxValue = 10000)] int amountOfLinens, 
            [RangeInt(MaxValue = 50000)] int amountOfTea);     
         
        IEnumerable<FlightDto> GetFlightsForRoute(int routeId);

        IEnumerable<WagonDto> GetWagonsForFlight(int flightId);

        FlightDto GetFlightForPlace(int placeId);
    }
}
