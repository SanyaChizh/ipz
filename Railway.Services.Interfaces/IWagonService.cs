﻿using System.Collections.Generic;
using Railway.Services.Dto;
using Railway.Services.Interfaces.Validators;
using static Railway.Domain.Core.Models.Wagon;

namespace Railway.Services.Interfaces
{
    public interface IWagonService : IDomainEntityService<WagonDto>
    {
        int Create(
            [ExistenEnum] WagonType type, 
            int stationId, 
            [RangeInt(MaxValue = 200)] int amountofPlaces);

        IEnumerable<PlaceDto> GetPlacesForWagon(int wagonId);
    }
}
