﻿using System;

namespace Railway.Services.Interfaces.Utils
{
    public class TrainStopInformation
    {
        public string Station { get; set; }

        public DateTime DepartureDate { get; set; }

        public DateTime ArrivalDate { get; set; }
    }
}
