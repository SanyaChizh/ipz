﻿using Railway.Services.Dto;
using Railway.Services.Interfaces.Validators;

namespace Railway.Services.Interfaces
{
    public interface IDriverService : IDomainEntityService<DriverDto>
    {
        int Create(
            [NonEmptyString] string firstName, 
            [NonEmptyString] string lastName, 
            int stationId);
    }
}
