﻿using Railway.Domain.Core.Enums;
using Railway.Services.Dto;
using Railway.Services.Interfaces.Validators;

namespace Railway.Services.Interfaces
{
    public interface IBoughtTicketService : IDomainEntityService<TicketDto>
    {
        int BuyTicket(
            int clientId, 
            int placeId, 
            int departurePointId, 
            int arrivalPointId,
            [ExistenEnum] TicketType type, 
            bool isExistsBaggage, 
            [RangeInt(MaxValue = 10)] int amountOfTea, 
            [RangeInt(MaxValue = 2)] int amountOfLinens, 
            [RangeInt(MaxValue = 200)] int age, 
            string studentNumber);

        float GetTicketPrice(
            int placeId,
            int departurePointId,
            int arrivalPointId,
            [RangeInt(MaxValue = 10)] int amountOfTea,
            [RangeInt(MaxValue = 2)] int amountOfLinens,
            bool isExistsBaggage,
            [ExistenEnum] TicketType ticketType);
    }
}
