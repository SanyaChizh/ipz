﻿using System.Collections.Generic;
using Railway.Services.Dto;
using Railway.Services.Interfaces.Validators;

namespace Railway.Services.Interfaces
{
    public interface ITrainService : IDomainEntityService<TrainDto>
    {
        int Create(
            int stationId, 
            int driverId, 
            [RangeInt] int speed, 
            IList<int> wagonsIds);
    }
}
