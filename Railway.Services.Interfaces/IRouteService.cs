﻿using System.Collections.Generic;
using Railway.Services.Dto;

namespace Railway.Services.Interfaces
{
    public interface IRouteService : IDomainEntityService<RouteDto>
    {
        IDictionary<string, int> GetStationsForRouteByPlace(int placeId);

        int CreateRoute(IList<int> trainStopIds);

        IEnumerable<RouteDto> GetRoutes();

        double GetRouteDistance(int routeId);

        double GetRouteDistance(int routeId, int firstStationId, int lastStationId);

        double GetRouteDistance(int routeId, string firstStation, string lastStation);
    }
}
