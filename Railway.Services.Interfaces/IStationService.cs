﻿using Railway.Services.Dto;
using Railway.Services.Interfaces.Validators;

namespace Railway.Services.Interfaces
{
    public interface IStationService : IDomainEntityService<StationDto>
    {
        int Create(
            [NonEmptyString] string name, 
            [NonEmptyString] string city);
    }
}
