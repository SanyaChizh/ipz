﻿using System;
using Railway.Services.Interfaces.Validators;

namespace Railway.Services.Interfaces
{
    public interface IPaymentService
    {
        void Pay(
            [NonEmptyString] string cardNumber, 
            [FutureDate] DateTime validity, 
            [NonEmptyString] string cvv2);
    }
}
