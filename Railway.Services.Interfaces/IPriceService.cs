﻿using Railway.Services.Dto;
using Railway.Services.Interfaces.Validators;

namespace Railway.Services.Interfaces
{
    public interface IPriceService : IDomainEntityService<PriceDataDto>
    {
        int Create(
            [NonEmptyString] string name, 
            [RangeFloat] float price);

        PriceDataDto Get(
            [NonEmptyString] string name);
    }
}
