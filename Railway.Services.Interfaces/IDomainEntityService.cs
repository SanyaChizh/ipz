﻿using System.Collections.Generic;

namespace Railway.Services.Interfaces
{
    public interface IDomainEntityService<out TDto>
    {
        TDto Get(int id);

        IEnumerable<int> GetAllIds();
    }
}
