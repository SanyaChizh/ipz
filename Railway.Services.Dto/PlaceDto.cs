﻿using static Railway.Domain.Core.Models.Place;

namespace Railway.Services.Dto
{
    public class PlaceDto
    {
        public PlaceDto(int id, PlaceState state, int placeNumber, int wagonId)
        {
            Id = id;
            State = state;
            PlaceNumber = placeNumber;
            WagonId = wagonId;
        }

        public int Id { get; private set; }

        public PlaceState State { get; set; }

        public int PlaceNumber { get; private set; }

        public int WagonId { get; private set; }
    }
}
