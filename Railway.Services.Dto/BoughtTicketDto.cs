﻿using System;

namespace Railway.Services.Dto
{
    public class BoughtTicketDto : TicketDto
    {
        public BoughtTicketDto(
            int id,
            int clientId,
            int placeId,
            string departurePoint,
            string arrivalPoint,
            string fiscalReceiptNumber,
            DateTime purchaseDate,
            TicketAttributesDto attributes) : base(id, clientId, placeId, departurePoint, arrivalPoint)
        {
            FiscalReceiptNumber = fiscalReceiptNumber;
            PurchaseDate = purchaseDate;
            Attributes = attributes;
        }

        public string FiscalReceiptNumber { get; private set; }

        public DateTime PurchaseDate { get; private set; }

        public TicketAttributesDto Attributes { get; private set; }
    }
}
