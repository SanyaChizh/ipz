﻿using System.Collections.Generic;

namespace Railway.Services.Dto
{
    public class ShoppingCartDto
    {
        public ShoppingCartDto(int id, ICollection<TicketDto> tickets)
        {
            Id = id;
            Tickets = tickets;
        }

        public int Id { get; private set; }

        public virtual ICollection<TicketDto> Tickets { get; private set; }
    }
}
