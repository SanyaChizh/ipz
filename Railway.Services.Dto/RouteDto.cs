﻿using System.Collections.Generic;

namespace Railway.Services.Dto
{
    public class RouteDto
    {
        public RouteDto(
            int id,
            string departurePoint,
            string arrivalPoint,
            IList<TrainStopDto> stops,
            IList<FlightDto> flights)
        {
            Id = id;
            DeparturePoint = departurePoint;
            ArrivalPoint = arrivalPoint;
            Stops = stops;
            Flights = flights;
        }

        public int Id { get; private set; }

        public string DeparturePoint { get; private set; }

        public string ArrivalPoint { get; private set; }

        public IList<TrainStopDto> Stops { get; private set; }

        public IList<FlightDto> Flights { get; private set; }
    }
}
