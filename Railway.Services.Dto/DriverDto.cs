﻿namespace Railway.Services.Dto
{
    public class DriverDto
    {
        public DriverDto(int id, string firstName, string lastName, int? trainId, int? stationId)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            TrainId = trainId;
            StationId = stationId;
        }

        public int Id { get; private set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public int? TrainId { get; private set; }

        public int? StationId { get; private set; }
    }
}
