﻿using System.Collections.Generic;
using static Railway.Domain.Core.Models.Wagon;

namespace Railway.Services.Dto
{
    public class WagonDto
    {
        public WagonDto(
            int id,
            WagonType type,
            int? trainId,
            int? stationId,
            ICollection<PlaceDto> places,
            int amountOfPlaces,
            int wagonNumber)
        {
            Id = id;
            Type = type;
            TrainId = trainId;
            StationId = stationId;
            Places = places;
            AmountOfPlaces = amountOfPlaces;
            WagonNumber = wagonNumber;
        }

        public int Id { get; private set; }

        public WagonType Type { get; private set; }

        public int? TrainId { get; private set; }

        public int? StationId { get; private set; }

        public ICollection<PlaceDto> Places { get; private set; }

        public int AmountOfPlaces { get; private set; }

        public int WagonNumber { get; set; }
    }
}
