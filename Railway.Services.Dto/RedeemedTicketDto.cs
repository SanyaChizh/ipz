﻿using System;

namespace Railway.Services.Dto
{
    public class RedeemedTicketDto : ReservedTicketDto
    {
        public RedeemedTicketDto(
            int id,
            int clientId,
            int placeId,
            string departurePoint,
            string arrivalPoint,
            DateTime reservationDate,
            string fiscalReceiptNumber,
            DateTime purchaseDate,
            TicketAttributesDto attributes) : base(id, clientId, placeId, departurePoint, arrivalPoint, reservationDate)
        {
            FiscalReceiptNumber = fiscalReceiptNumber;
            PurchaseDate = purchaseDate;
            Attributes = attributes;
        }

        public string FiscalReceiptNumber { get; private set; }

        public DateTime PurchaseDate { get; private set; }

        public TicketAttributesDto Attributes { get; private set; }
    }
}
