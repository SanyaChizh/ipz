﻿namespace Railway.Services.Dto
{
    public class PriceDataDto 
    {
        public PriceDataDto(int id, string name, float value)
        {
            Id = id;
            Name = name;
            Value = value;
        }

        public int Id { get; private set; }

        public string Name { get; private set; }

        public float Value { get; private set; }
    }
}
