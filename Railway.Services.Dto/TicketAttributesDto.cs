﻿using Railway.Domain.Core.Enums;

namespace Railway.Services.Dto
{
    public class TicketAttributesDto
    {
        public TicketAttributesDto(
            TicketType type,
            string studentNumber,
            bool isExistsBaggage,
            int age,
            int amountOfTea,
            int amountOfLinens,
            float price)
        {
            Type = type;
            StudentNumber = studentNumber;
            IsExistsBaggage = isExistsBaggage;
            Age = age;
            AmountOfTea = amountOfTea;
            AmountOfLinens = AmountOfLinens;
            Price = price;
        }

        public TicketType Type { get; private set; }

        public string StudentNumber { get; private set; }

        public bool IsExistsBaggage { get; private set; }

        public int Age { get; private set; }

        public int AmountOfTea { get; private set; }

        public int AmountOfLinens { get; private set; }

        public float Price { get; private set; }
    }
}
