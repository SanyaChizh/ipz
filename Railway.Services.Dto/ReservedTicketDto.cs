﻿using System;

namespace Railway.Services.Dto
{
    public class ReservedTicketDto : TicketDto
    {
        public ReservedTicketDto(int id, int clientId, int placeId, string departurePoint, string arrivalPoint, DateTime reservationDate)
            : base(id, clientId, placeId, departurePoint, arrivalPoint)
        {
            ReservetionDate = reservationDate;
        }

        public DateTime ReservetionDate { get; private set; }
    }
}
