﻿using System.Linq;
using Railway.Domain.Core.Models;
using Railway.Exceptions.Service;

namespace Railway.Services.Dto.Utils
{
    public static class DtoBuilder
    {
        public static BoughtTicketDto ToDto(this BoughtTicket ticket)
        {
            return new BoughtTicketDto(
                ticket.Id,
                ticket.Client.Id,
                ticket.Place.Id,
                ticket.DeparturePoint,
                ticket.ArrivalPoint,
                ticket.FiscalReceiptNumber,
                ticket.PurchaseDate,
                ticket.Attributes.ToDto());
        }

        public static ClientDto ToDto(this Client client)
        {
            return new ClientDto(
                client.Id,
                client.FirstName,
                client.LastName,
                client.Email);
        }

        public static DriverDto ToDto(this Driver driver)
        {
            int? trainId = null;
            int? stationId = null;
            if (driver.Train != null)
            {
                trainId = driver.Train.Id;
            }

            if (driver.Station != null)
            {
                stationId = driver.Station.Id;
            }

            return new DriverDto(
                driver.Id,
                driver.FirstName,
                driver.LastName,
                trainId,
                stationId);
        }

        public static FlightDto ToDto(this Flight flight)
        {
            return new FlightDto(
                flight.Id,
                flight.State,
                flight.DepartureDate,
                flight.ArrivalDate,
                flight.Train.Id,
                flight.AmountOfLinens,
                flight.AmountOfTea,
                flight.Route.Id);
        }

        public static PlaceDto ToDto(this Place place)
        {
            return new PlaceDto(
                place.Id,
                place.State,
                place.Wagon.Places.ToList().IndexOf(place) + 1,
                place.Wagon.Id);
        }

        public static PriceDataDto ToDto(this PriceData priceData)
        {
            return new PriceDataDto(
                priceData.Id,
                priceData.Name,
                priceData.Value);
        }

        public static RedeemedTicketDto ToDto(this RedeemedTicket ticket)
        {
            return new RedeemedTicketDto(
                ticket.Id,
                ticket.Client.Id,
                ticket.Place.Id,
                ticket.DeparturePoint,
                ticket.ArrivalPoint,
                ticket.ReservetionDate,
                ticket.FiscalReceiptNumber,
                ticket.PurchaseDate,
                ticket.Attributes.ToDto());
        }

        public static ReservedTicketDto ToDto(this ReservedTicket ticket)
        {
            return new ReservedTicketDto(
                ticket.Id,
                ticket.Client.Id,
                ticket.Place.Id,
                ticket.DeparturePoint,
                ticket.ArrivalPoint,
                ticket.ReservetionDate);
        }

        public static RouteDto ToDto(this Route route)
        {
            return new RouteDto(
                route.Id,
                route.DeparturePoint,
                route.ArrivalPoint,
                route.Stops.Select(s => s.ToDto()).ToList(),
                route.Flights.Select(f => f.ToDto()).ToList());
        }

        public static ShoppingCartDto ToDto(this ShoppingCart shoppingCart)
        {
            return new ShoppingCartDto(
                shoppingCart.Id,
                shoppingCart.Tickets.Select(t => t.ToDto()).ToList());
        }

        public static StationDto ToDto(this Station station)
        {
            return new StationDto(
                station.Id,
                station.Name,
                station.City,
                station.Trains.Select(t => t.Id).ToList(),
                station.Wagons.Select(w => w.Id).ToList(),
                station.Drivers.Select(d => d.Id).ToList());
        }

        public static StationsDistanceDto ToDto(this StationsDistance stationDistance)
        {
            return new StationsDistanceDto(
                stationDistance.Id,
                stationDistance.Stations.Select(s => s.ToDto()).ToList(),
                stationDistance.Distance);
        }

        public static TrainDto ToDto(this Train train)
        {
            int? stationId = null;
            int? flightId = null;
            if (train.Station != null)
            {
                stationId = train.Station.Id;
            }

            if (train.Flight != null)
            {
                flightId = train.Flight.Id;
            }

            return new TrainDto(
                train.Id,
                stationId,
                flightId,
                train.Driver.Id,
                train.Wagons.Select(t => t.ToDto()).ToList(),
                train.Speed);
        }

        public static TrainStopDto ToDto(this TrainStop trainStop)
        {
            return new TrainStopDto(
                trainStop.Id,
                trainStop.Station.Id,
                trainStop.Time);
        }

        public static WagonDto ToDto(this Wagon wagon)
        {
            int? stationId = null;
            if (wagon.Station != null)
            {
                stationId = wagon.Station.Id;
            }

            int wagonIndex = wagon.Train.Wagons.ToList().IndexOf(wagon);

            return new WagonDto(
                wagon.Id,
                wagon.Type,
                wagon.Train.Id,
                stationId,
                wagon.Places.Select(p => p.ToDto()).ToList(),
                wagon.AmountOfPlaces,
                wagonIndex + 1);
        }

        public static TicketDto ToDto(this Ticket ticket)
        {
            var ticketType = ticket.GetType().BaseType;
            if (ticketType == typeof(RedeemedTicket))
            {
                return (ticket as RedeemedTicket).ToDto();
            }

            if (ticketType == typeof(BoughtTicket))
            {
                return (ticket as BoughtTicket).ToDto();
            }

            if (ticketType == typeof(ReservedTicket))
            {
                return (ticket as ReservedTicket).ToDto();
            }

            throw new NotDefinedTicketTypeException();
        }

        private static TicketAttributesDto ToDto(this TicketAttributes attributes)
        {
            return new TicketAttributesDto(
                attributes.Type,
                attributes.StudentNumber,
                attributes.IsExistsBaggage,
                attributes.Age,
                attributes.AmountOfTea,
                attributes.AmountOfLinens,
                attributes.Price);
        }
    }
}
