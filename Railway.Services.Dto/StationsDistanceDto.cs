﻿using System.Collections.Generic;

namespace Railway.Services.Dto
{
    public class StationsDistanceDto 
    {
        public StationsDistanceDto(int id, ICollection<StationDto> stations, double distance)
        {
            Id = id;
            Stations = stations;
            Distance = distance;
        }

        public int Id { get; private set; }

        public ICollection<StationDto> Stations { get; private set; }

        public double Distance { get; private set; }
    }
}
