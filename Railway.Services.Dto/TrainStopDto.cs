﻿using System;

namespace Railway.Services.Dto
{
    public class TrainStopDto
    {
        public TrainStopDto(int id, int stationId, TimeSpan time)
        {
            Id = id;
            StationId = stationId;
            Time = time;
        }

        public int Id { get; private set; }

        public int StationId { get; private set; }

        public TimeSpan Time { get; private set; }
    }
}
