﻿using System.Collections.Generic;

namespace Railway.Services.Dto
{
    public class StationDto
    {
        public StationDto(
            int id,
            string name,
            string city,
            ICollection<int> trains,
            ICollection<int> wagons,
            ICollection<int> drivers)
        {
            Id = id;
            Name = name;
            City = city;
            Trains = trains;
            Wagons = wagons;
            Drivers = drivers;
        }

        public int Id { get; private set; }

        public string Name { get; private set; }

        public string City { get; private set; }

        public string FullName => $"{City} ({Name})";

        public virtual ICollection<int> Trains { get; private set; }

        public virtual ICollection<int> Wagons { get; private set; }

        public virtual ICollection<int> Drivers { get; private set; }
    }
}
