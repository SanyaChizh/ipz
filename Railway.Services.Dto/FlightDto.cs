﻿using System;
using static Railway.Domain.Core.Models.Flight;

namespace Railway.Services.Dto
{
    public class FlightDto
    {
        public FlightDto(
            int id,
            FlightState state,
            DateTime departureDate,
            DateTime arrivalDate,
            int trainId,
            int amountOfLinens,
            int amountOfTea,
            int routeId)
        {
            Id = id;
            State = state;
            DepartureDate = departureDate;
            ArrivalDate = arrivalDate;
            TrainId = trainId;
            AmountOfLinens = amountOfLinens;
            AmountOfTea = amountOfTea;
            RouteId = routeId;
        }

        public int Id { get; private set; }

        public FlightState State { get; private set; }

        public DateTime DepartureDate { get; private set; }

        public DateTime ArrivalDate { get; private set; }

        public int TrainId { get; private set; }

        public int RouteId { get; private set; }

        public int AmountOfLinens { get; private set; }

        public int AmountOfTea { get; private set; }
    }
}
