﻿using System.Collections.Generic;

namespace Railway.Services.Dto
{
    public class TrainDto
    {
        public TrainDto(
            int id,
            int? stationId,
            int? flightId,
            int driverId,
            ICollection<WagonDto> wagons,
            int speed)
        {
            Id = id;
            StationId = stationId;
            FlightId = flightId;
            DriverId = driverId;
            Wagons = wagons;
            Speed = speed;
        }

        public int Id { get; private set; }

        public int? StationId { get; private set; }

        public int? FlightId { get; private set; }

        public int DriverId { get; private set; }

        public ICollection<WagonDto> Wagons { get; private set; }

        public int Speed { get; private set; }
    }
}
