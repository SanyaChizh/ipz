﻿namespace Railway.Services.Dto
{
    public abstract class TicketDto
    {
        protected TicketDto(int id, int clientId, int placeId, string departurePoint, string arrivalPoint)
        {
            Id = id;
            ClientId = clientId;
            PlaceId = placeId;
            DeparturePoint = departurePoint;
            ArrivalPoint = arrivalPoint;
        }

        public int Id { get; private set; }

        public int ClientId { get; private set; }

        public int PlaceId { get; private set; }

        public string DeparturePoint { get; private set; }

        public string ArrivalPoint { get; private set; }
    }
}
