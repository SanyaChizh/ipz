﻿using System.Collections.Generic;
using Ninject;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using Railway.Exceptions.Service;
using Railway.Services.Dto;
using Railway.Services.Dto.Utils;
using Railway.Services.Interfaces;

namespace Railway.Infrastructure.Services
{
    public class DriverService : IDriverService
    {
        [Inject]
        public IUnitOfWork UnitOfWork { private get; set; }

        public IEnumerable<int> GetAllIds()
        {
            return UnitOfWork.Drivers.Select(d => d.Id);
        }

        public int Create(string firstName, string lastName, int stationId)
        {
            Station station = GetNotNullStation(stationId);
            Driver driver = new Driver(firstName, lastName, null, station);
            UnitOfWork.Drivers.Create(driver);
            UnitOfWork.Save();

            return driver.Id;
        }

        public DriverDto Get(int id)
        {
            var driver = GetNotNullDriver(id);

            return driver.ToDto();
        }

        private Station GetNotNullStation(int id)
        {
            var station = UnitOfWork.Stations.Get(id);
            if (station == null)
            {
                throw new EntityNotFoundException(typeof(Station));
            }

            return station;
        }

        private Driver GetNotNullDriver(int id)
        {
            var driver = UnitOfWork.Drivers.Get(id);
            if (driver == null)
            {
                throw new EntityNotFoundException(typeof(Driver));
            }

            return driver;
        }
    }
}
