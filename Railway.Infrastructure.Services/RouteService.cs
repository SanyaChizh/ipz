﻿using System.Collections.Generic;
using System.Linq;
using Ninject;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using Railway.Exceptions.Service;
using Railway.Services.Dto;
using Railway.Services.Dto.Utils;
using Railway.Services.Interfaces;

namespace Railway.Infrastructure.Services
{
    public class RouteService : IRouteService
    {
        [Inject]
        public IStationDistanceService StationDistanceService { private get; set; }

        [Inject]
        public IUnitOfWork UnitOfWork { private get; set; }

        public IEnumerable<int> GetAllIds()
        {
            return UnitOfWork.Routes.Select(r => r.Id);
        }

        public IDictionary<string, int> GetStationsForRouteByPlace(int placeId)
        {
            IDictionary<string, int> stations = new Dictionary<string, int>();

            Place place = UnitOfWork.Places.Get(placeId);
            if (place == null)
            {
                throw new EntityNotFoundException(typeof(Place));
            }

            Train train = place.Wagon.Train;
            if (train == null)
            {
                throw new EntityNotFoundException(typeof(Train));
            }

            Flight flight = place.Wagon.Train.Flight;
            if (flight == null)
            {
                throw new EntityNotOnFlightException(typeof(Flight));
            }

            Route route = flight.Route;
            foreach (TrainStop ts in route.Stops)
            {
                stations[ts.Station.FullName] = ts.Station.Id;
            }

            return stations;
        }

        public int CreateRoute(IList<int> trainStopIds)
        {
            if (trainStopIds.Count < 2)
            {
                throw new InvalidModelStateForOperationException("Route must have minimum 2 train stops");
            }

            var route = new Route();
            foreach (var id in trainStopIds)
            {
                var ts = GetNotNullTrainStop(id);
                route.Stops.Add(ts);
            }

            UnitOfWork.Routes.Create(route);
            UnitOfWork.Save();

            return route.Id;
        }

        public RouteDto Get(int id)
        {
            Route route = GetNotNullRoute(id);

            return route.ToDto();
        }

        public IEnumerable<RouteDto> GetRoutes()
        {
            return UnitOfWork.Routes.GetAll().Select(r => r.ToDto());
        }

        public double GetRouteDistance(int routeId)
        {
            Route route = GetNotNullRoute(routeId);

            return GetDistanceForTrainStops(route.Stops.ToList());
        }

        public double GetRouteDistance(int routeId, int firstStationId, int lastStationId)
        {
            Route route = GetNotNullRoute(routeId);
            IList<TrainStop> stops = GetCutByStationsTrainStopsListForRoute(route, firstStationId, lastStationId);

            return GetDistanceForTrainStops(stops);
        }

        public double GetRouteDistance(int routeId, string firstStation, string lastStation)
        {
            Route route = GetNotNullRoute(routeId);
            IList<TrainStop> stops = GetCutByStationsTrainStopsListForRoute(route, firstStation, lastStation);

            return GetDistanceForTrainStops(stops);
        }

        private double GetDistanceForTrainStops(IList<TrainStop> stops)
        {
            double distance = 0;
            for (var i = 0; i < stops.Count - 1; i++)
            {
                var stationId = stops[i].Station.Id;
                var nextStationId = stops[i + 1].Station.Id;
                var distanceDto = StationDistanceService.FindStationDistance(stationId, nextStationId);
                distance += distanceDto.Distance;
            }

            return distance;
        }

        private IList<TrainStop> GetCutByStationsTrainStopsListForRoute(Route route, int firstStationId, int lastStationId)
        {
            List<TrainStop> stops = route.Stops.ToList();
            if (stops.Count(ts => ts.Station.Id == firstStationId || ts.Station.Id == lastStationId) < 2)
            {
                throw new EntityNotFoundException(typeof(TrainStop), "Train stop not found for this route");
            }

            stops = stops.SkipWhile(ts => ts.Station.Id != firstStationId).ToList();
            stops = stops.TakeWhile(ts => ts.Station.Id != lastStationId).ToList();
            stops.Add(route.Stops.First(ts => ts.Station.Id == lastStationId));

            return stops;
        }

        private IList<TrainStop> GetCutByStationsTrainStopsListForRoute(Route route, string firstStation, string lastStation)
        {
            List<TrainStop> stops = route.Stops.ToList();
            if (stops.Count(ts => ts.Station.FullName == firstStation || ts.Station.FullName == lastStation) < 2)
            {
                throw new EntityNotFoundException(typeof(TrainStop), "Train stop not found for this route");
            }

            stops = stops.SkipWhile(ts => ts.Station.FullName != firstStation).ToList();
            stops = stops.TakeWhile(ts => ts.Station.FullName != lastStation).ToList();
            stops.Add(route.Stops.First(ts => ts.Station.FullName == lastStation));

            return stops;
        }

        private Route GetNotNullRoute(int id)
        {
            Route route = UnitOfWork.Routes.Get(id);
            if (route == null)
            {
                throw new EntityNotFoundException(typeof(Route));
            }

            return route;
        }

        private TrainStop GetNotNullTrainStop(int id)
        {
            TrainStop trainStop = UnitOfWork.TrainStops.Get(id);
            if (trainStop == null)
            {
                throw new EntityNotFoundException(typeof(TrainStop));
            }

            return trainStop;
        }
    }
}
