﻿using System.Collections.Generic;
using Ninject;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using Railway.Exceptions.Service;
using Railway.Services.Dto;
using Railway.Services.Dto.Utils;
using Railway.Services.Interfaces;

namespace Railway.Infrastructure.Services
{
    public class TrainService : ITrainService
    {
        [Inject]
        public IUnitOfWork UnitOfWork { private get; set; }

        public int Create(int stationId, int driverId, int speed, IList<int> wagonsIds)
        {
            Station station = UnitOfWork.Stations.Get(stationId);
            if (station == null)
            {
                throw new EntityNotFoundException(typeof(Station));
            }

            Driver driver = GetNotNullDriver(driverId);
            if (driver.Station != station)
            {
                throw new InvalidModelStateForOperationException("Driver must be on selected station");
            }

            driver.Station = null;
            var train = new Train(station, null, driver, speed);

            foreach (int t in wagonsIds)
            {
                Wagon wagon = GetNotNullWagon(t);
                if (wagon.Station != station)
                {
                    throw new InvalidModelStateForOperationException("Wagon must be on selected station");
                }

                wagon.Station = null;
                train.Wagons.Add(wagon);
            }

            UnitOfWork.Stations.Update(station);
            UnitOfWork.Trains.Create(train);
            UnitOfWork.Save();

            return train.Id;
        }

        public TrainDto Get(int id)
        {
            Train train = GetNotNullTrain(id);

            return train.ToDto();
        }

        public IEnumerable<int> GetAllIds()
        {
            return UnitOfWork.Trains.Select(t => t.Id);
        }

        private Wagon GetNotNullWagon(int id)
        {
            Wagon wagon = UnitOfWork.Wagons.Get(id);
            if (wagon == null)
            {
                throw new EntityNotFoundException(typeof(Wagon));
            }

            return wagon;
        }

        private Driver GetNotNullDriver(int id)
        {
            Driver driver = UnitOfWork.Drivers.Get(id);
            if (driver == null)
            {
                throw new EntityNotFoundException(typeof(Driver));
            }

            return driver;
        }

        private Train GetNotNullTrain(int id)
        {
            Train train = UnitOfWork.Trains.Get(id);
            if (train == null)
            {
                throw new EntityNotFoundException(typeof(Train));
            }

            return train;
        }
    }
}
