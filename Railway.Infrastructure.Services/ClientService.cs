﻿using System.Collections.Generic;
using System.Linq;
using Ninject;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using Railway.Exceptions.Service;
using Railway.Services.Dto;
using Railway.Services.Dto.Utils;
using Railway.Services.Interfaces;

namespace Railway.Infrastructure.Services
{
    public class ClientService : IClientService
    {
        [Inject]
        public IUnitOfWork UnitOfWork { private get; set; }

        public int Create(string firstName, string lastName, string email)
        {
            var client = new Client(firstName, lastName, email);
            UnitOfWork.Clients.Create(client);
            UnitOfWork.Save();

            return client.Id;
        }

        public ClientDto Get(int id)
        {
            var client = UnitOfWork.Clients.Get(id);
            if (client == null)
            {
                throw new EntityNotFoundException(typeof(Client));
            }

            return client.ToDto();
        }

        public ClientDto GetClient(string firstName, string lastName, string email)
        {
            var clients = UnitOfWork.Clients.Find(c => c.FirstName == firstName && 
                                                       c.LastName == lastName && 
                                                       c.Email == email) as List<Client>;
            if (clients == null || clients.Count == 0)
            {
                throw new EntityNotFoundException(typeof(Client));
            }

            return clients.First().ToDto();
        }

        public IEnumerable<int> GetAllIds()
        {
            return UnitOfWork.Clients.Select(c => c.Id);
        }
    }
}
