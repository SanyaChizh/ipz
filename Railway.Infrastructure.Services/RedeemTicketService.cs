﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using Railway.Domain.Core.Enums;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using Railway.Exceptions.Service;
using Railway.Services.Dto;
using Railway.Services.Dto.Utils;
using Railway.Services.Interfaces;
using static Railway.Domain.Core.Models.Wagon;

namespace Railway.Infrastructure.Services
{
    public class RedeemTicketService : IRedeemTicketService
    {
        [Inject]
        public IPriceService PriceService { private get; set; }

        [Inject]
        public IRouteService RouteService { private get; set; }

        [Inject]
        public IUnitOfWork UnitOfWork { private get; set; }

        public int RedeemTicket(int reservedTicketId, bool isExistsBaggage, int amountOfTea, int amountOfLinens)
        {
            ReservedTicket reservedTicket = UnitOfWork.ReservedTickets.Get(reservedTicketId);
            if (reservedTicket == null)
            {
                throw new EntityNotFoundException(typeof(ReservedTicket));
            }

            int routeId = reservedTicket.Place.Wagon.Train.Flight.Route.Id;
            string departurePoint = reservedTicket.DeparturePoint;
            string arrivalPoint = reservedTicket.ArrivalPoint;
            WagonType wagonType = reservedTicket.Place.Wagon.Type;
            float ticketPrice = GetTicketPrice(isExistsBaggage, routeId, departurePoint, arrivalPoint, amountOfTea, amountOfLinens, wagonType);
            var fiscalReceiptNumber = "123";
            var ticketAttributes = new TicketAttributes(TicketType.Full, string.Empty, isExistsBaggage, 0, amountOfTea, amountOfLinens, ticketPrice);
            var ticket = new RedeemedTicket(
                reservedTicket.Client, 
                reservedTicket.Place, 
                departurePoint, 
                arrivalPoint,
                reservedTicket.ReservetionDate, 
                fiscalReceiptNumber,
                DateTime.UtcNow, 
                ticketAttributes);

            reservedTicket.Place.State = Place.PlaceState.Busy;
            UnitOfWork.Places.Update(reservedTicket.Place);
            UnitOfWork.RedeemedTickets.Create(ticket);
            UnitOfWork.ReservedTickets.Delete(reservedTicket);
            UnitOfWork.Save();

            return ticket.Id;
        }

        public TicketDto Get(int id)
        {
            Ticket ticket = UnitOfWork.RedeemedTickets.Get(id);
            if (ticket == null)
            {
                throw new EntityNotFoundException(typeof(RedeemedTicket));
            }

            return ticket.ToDto();
        }

        public IEnumerable<int> GetAllIds()
        {
            return UnitOfWork.ReservedTickets
                .GetAll()
                .ToList()
                .Where(t => t.GetType() == typeof(RedeemedTicket))
                .Select(t => t.Id);
        }

        private float GetTicketPrice(
            bool isExistsBaggage, 
            int routeId, 
            string departurePoint,
            string arrivalPoint, 
            int amountOfTea,
            int amountOfLinens,
            WagonType wagonType)
        {
            float totalPrice = PriceService.Get("TicketReservation").Value;
            if (isExistsBaggage)
            {
                totalPrice += PriceService.Get("Baggage").Value;
            }

            double routeDistance = RouteService.GetRouteDistance(routeId, departurePoint, arrivalPoint);
            totalPrice += (float)(routeDistance * PriceService.Get("PriceForOneKilometer").Value);
            totalPrice += amountOfTea * PriceService.Get("Tea").Value;
            totalPrice += amountOfLinens * PriceService.Get("Linen").Value;

            switch (wagonType)
            {
                case WagonType.SittingSecondClass:
                    totalPrice *= PriceService.Get("PriceСoefficientForSittingSecondClass").Value;
                    break;
                case WagonType.SittingFirstClass:
                    totalPrice *= PriceService.Get("PriceСoefficientForSittingFirstClass").Value;
                    break;
                case WagonType.ReservedSeat:
                    totalPrice *= PriceService.Get("PriceСoefficientForReservedSeat").Value;
                    break;
                case WagonType.Compartment:
                    totalPrice *= PriceService.Get("PriceСoefficientForCompartment").Value;
                    break;
                case WagonType.Lux:
                    totalPrice *= PriceService.Get("PriceСoefficientForLux").Value;
                    break;
            }
    
            return (float)Math.Round(totalPrice, 2);
        }
    }
}
