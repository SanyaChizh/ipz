﻿using System.Collections.Generic;
using System.Linq;
using Ninject;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using Railway.Exceptions.Service;
using Railway.Services.Dto;
using Railway.Services.Dto.Utils;
using Railway.Services.Interfaces;

namespace Railway.Infrastructure.Services
{
    public class StationDistanceService : IStationDistanceService
    {
        [Inject]
        public IUnitOfWork UnitOfWork { private get; set; }

        public IEnumerable<int> GetAllIds()
        {
            return UnitOfWork.StationsDistances.Select(sd => sd.Id);
        }

        public int Create(int firstStationId, int secondStationId, double distance)
        {
            StationsDistance sd = GetStationDistanceOrNull(firstStationId, secondStationId);
            if (sd != null)
            {
                throw new DoubleAddedEntityException("Station distance for this stations was defined");
            }

            Station firstStation = GetNotNullStation(firstStationId);
            Station secondStation = GetNotNullStation(secondStationId);
            var stationDistance = new StationsDistance(firstStation, secondStation, distance);
            UnitOfWork.StationsDistances.Create(stationDistance);
            UnitOfWork.Save();

            return stationDistance.Id;
        }

        public StationsDistanceDto FindStationDistance(int firstStationId, int secondStationId)
        {
            StationsDistance distance = GetStationDistanceOrNull(firstStationId, secondStationId);
            if (distance == null)
            {
                throw new EntityNotFoundException(typeof(StationsDistance), "Station distance not defined");
            }

            return distance.ToDto();
        }

        public StationsDistanceDto Get(int id)
        {
            StationsDistance sd = GetNotNullStationDistance(id);

            return sd.ToDto();
        }

        private StationsDistance GetStationDistanceOrNull(int firstStationId, int secondStationId)
        {
            StationsDistance distance = UnitOfWork.StationsDistances
                .GetAll()
                .ToList()
                .FirstOrDefault(sd => sd.Stations.First().Id == firstStationId &&
                                      sd.Stations.Last().Id == secondStationId);
            return distance;
        }

        private StationsDistance GetNotNullStationDistance(int id)
        {
            StationsDistance sd = UnitOfWork.StationsDistances.Get(id);
            if (sd == null)
            {
                throw new EntityNotFoundException(typeof(StationsDistance));
            }

            return sd;
        }

        private Station GetNotNullStation(int id)
        {
            Station station = UnitOfWork.Stations.Get(id);
            if (station == null)
            {
                throw new EntityNotFoundException(typeof(Station));
            }

            return station;
        }
    }
}
