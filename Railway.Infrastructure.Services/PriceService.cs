﻿using System.Collections.Generic;
using System.Linq;
using Ninject;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using Railway.Exceptions.Service;
using Railway.Services.Dto;
using Railway.Services.Dto.Utils;
using Railway.Services.Interfaces;

namespace Railway.Infrastructure.Services
{
    public class PriceService : IPriceService
    {
        [Inject]
        public IUnitOfWork UnitOfWork { private get; set; }

        public int Create(string name, float price)
        {
            var priceData = new PriceData(name, price);
            UnitOfWork.PricesData.Create(priceData);
            UnitOfWork.Save();

            return priceData.Id;
        }

        public PriceDataDto Get(string name)
        {
            PriceData priceData = UnitOfWork.PricesData.Find(pd => pd.Name == name).FirstOrDefault();
            if (priceData == null)
            {
                throw new EntityNotFoundException(typeof(PriceData));
            }

            return priceData.ToDto();
        }

        public PriceDataDto Get(int id)
        {
            PriceData priceData = GetNotNullPriceData(id);

            return priceData.ToDto();
        }

        public IEnumerable<int> GetAllIds()
        {
            return UnitOfWork.PricesData.Select(pd => pd.Id);
        }

        private PriceData GetNotNullPriceData(int id)
        {
            PriceData priceData = UnitOfWork.PricesData.Get(id);
            if (priceData == null)
            {
                throw new EntityNotFoundException(typeof(PriceData));
            }

            return priceData;
        }
    }
}
