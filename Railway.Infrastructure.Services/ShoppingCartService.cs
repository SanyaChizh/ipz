﻿using System.Collections.Generic;
using Ninject;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using Railway.Exceptions.Service;
using Railway.Services.Dto;
using Railway.Services.Dto.Utils;
using Railway.Services.Interfaces;

namespace Railway.Infrastructure.Services
{
    public class ShoppingCartService : IShoppingCartService
    {
        [Inject]
        public IUnitOfWork UnitOfWork { private get; set; }

        public IEnumerable<int> GetAllIds()
        {
            return UnitOfWork.ShoppingCarts.Select(sc => sc.Id);
        }

        public void AddBoughtTicketToShoppingCart(int shoppingCartId, int ticketId)
        {
            BoughtTicket ticket = GetNotNullBoughtTicket(ticketId);

            AddTicketToShoppingCart(shoppingCartId, ticket);
        }

        public void AddReservedTicketToShoppingCart(int shoppingCartId, int ticketId)
        {
            ReservedTicket ticket = GetNotNullReservedTicket(ticketId);

            AddTicketToShoppingCart(shoppingCartId, ticket);
        }

        public ShoppingCartDto Get(int id)
        {
            ShoppingCart sc = GetNotNullShoppingCart(id);

            return sc.ToDto();
        }

        public int Create()
        {
            var shoppingCart = new ShoppingCart();
            UnitOfWork.ShoppingCarts.Create(shoppingCart);
            UnitOfWork.Save();

            return shoppingCart.Id;
        }

        private void AddTicketToShoppingCart(int shoppingCartId, Ticket ticket)
        {
            ShoppingCart sc = GetNotNullShoppingCart(shoppingCartId);
            if (sc.Tickets.Contains(ticket))
            {
                throw new DoubleAddedEntityException("Ticket already added in shopping cart");
            }

            sc.Tickets.Add(ticket);
            UnitOfWork.ShoppingCarts.Update(sc);
            UnitOfWork.Save();
        }

        private BoughtTicket GetNotNullBoughtTicket(int id)
        {
            BoughtTicket ticket = UnitOfWork.BoughtTickets.Get(id);
            if (ticket == null)
            {
                throw new EntityNotFoundException(typeof(BoughtTicket));
            }

            return ticket;
        }

        private ReservedTicket GetNotNullReservedTicket(int id)
        {
            ReservedTicket ticket = UnitOfWork.ReservedTickets.Get(id);
            if (ticket == null)
            {
                throw new EntityNotFoundException(typeof(ReservedTicket));
            }

            return ticket;
        }

        private ShoppingCart GetNotNullShoppingCart(int id)
        {
            ShoppingCart sc = UnitOfWork.ShoppingCarts.Get(id);
            if (sc == null)
            {
                throw new EntityNotFoundException(typeof(ShoppingCart));
            }

            return sc;
        }
    }
}
