﻿using System.Collections.Generic;
using Ninject;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using Railway.Exceptions.Service;
using Railway.Services.Dto;
using Railway.Services.Dto.Utils;
using Railway.Services.Interfaces;

namespace Railway.Infrastructure.Services
{
    public class StationService : IStationService
    {
        [Inject]
        public IUnitOfWork UnitOfWork { private get; set; }

        public IEnumerable<int> GetAllIds()
        {
            return UnitOfWork.Stations.Select(s => s.Id);
        }

        public int Create(string name, string city)
        {
            var station = new Station(name, city);
            UnitOfWork.Stations.Create(station);
            UnitOfWork.Save();

            return station.Id;
        }

        public StationDto Get(int id)
        {
            Station station = GetNotNullStation(id);

            return station.ToDto();
        }

        private Station GetNotNullStation(int id)
        {
            Station station = UnitOfWork.Stations.Get(id);
            if (station == null)
            {
                throw new EntityNotFoundException(typeof(Station));
            }

            return station;
        }
    }
}
