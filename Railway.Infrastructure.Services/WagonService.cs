﻿using System.Collections.Generic;
using System.Linq;
using Ninject;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using Railway.Exceptions.Service;
using Railway.Services.Dto;
using Railway.Services.Dto.Utils;
using Railway.Services.Interfaces;
using static Railway.Domain.Core.Models.Wagon;

namespace Railway.Infrastructure.Services
{
    public class WagonService : IWagonService
    {
        [Inject]
        public IUnitOfWork UnitOfWork { private get; set; }

        public int Create(WagonType type, int stationId, int amountOfPlaces)
        {
            Station station = UnitOfWork.Stations.Get(stationId);
            if (station == null)
            {
                throw new EntityNotFoundException(typeof(Station));
            }

            var wagon = new Wagon(type, null, station, amountOfPlaces);
            AddPlaces(wagon, wagon.AmountOfPlaces);       
            UnitOfWork.Wagons.Create(wagon);
            UnitOfWork.Save();

            return wagon.Id;
        }

        public WagonDto Get(int id)
        {
            Wagon wagon = GetNotNullWagon(id);

            return wagon.ToDto();
        }

        public IEnumerable<PlaceDto> GetPlacesForWagon(int wagonId)
        {
            Wagon wagon = GetNotNullWagon(wagonId);

            return wagon.Places.Select(p => p.ToDto()).ToList();
        }

        public IEnumerable<int> GetAllIds()
        {
            return UnitOfWork.Wagons.Select(w => w.Id);
        }

        private void AddPlaces(Wagon wagon, int amountOfPlaces)
        {
            for (var i = 0; i < amountOfPlaces; i++)
            {
                var place = new Place(Place.PlaceState.Free, wagon);
                wagon.Places.Add(place);
            }
        }

        private Wagon GetNotNullWagon(int id)
        {
            Wagon wagon = UnitOfWork.Wagons.Get(id);
            if (wagon == null)
            {
                throw new EntityNotFoundException(typeof(Wagon));
            }

            return wagon;
        }
    }
}
