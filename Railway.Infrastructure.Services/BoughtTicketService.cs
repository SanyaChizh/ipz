﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using Railway.Domain.Core.Enums;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using Railway.Exceptions.Service;
using Railway.Services.Dto;
using Railway.Services.Dto.Utils;
using Railway.Services.Interfaces;
using static Railway.Domain.Core.Models.Wagon;

namespace Railway.Infrastructure.Services
{
    public class BoughtTicketService : IBoughtTicketService
    {
        [Inject]
        public IPriceService PriceService { private get; set; }

        [Inject]
        public IRouteService RouteService { private get; set; }

        [Inject]
        public IUnitOfWork UnitOfWork { private get; set; }

        public IEnumerable<int> GetAllIds()
        {
            return UnitOfWork.BoughtTickets
                .GetAll()
                .ToList()
                .Where(t => t.GetType() == typeof(BoughtTicket))
                .Select(t => t.Id);
        }

        public int BuyTicket(
            int clientId,
            int placeId,
            int departurePointId,
            int arrivalPointId,
            TicketType type,
            bool isExistsBaggage,
            int amountOfTea, 
            int amountOfLinens,
            int age, 
            string studentNumber)
        {
            Client client = UnitOfWork.Clients.Get(clientId);
            if (client == null)
            {
                throw new EntityNotFoundException(typeof(Client));
            }

            Place place = UnitOfWork.Places.Get(placeId);
            if (place == null)
            {
                throw new EntityNotFoundException(typeof(Place));
            }

            if (place.State == Place.PlaceState.Busy || place.State == Place.PlaceState.Reserved)
            {
                throw new InvalidArgumentException();
            }

            WagonType wagonType = place.Wagon.Type;
            Route route = place.Wagon.Train.Flight.Route;
            float ticketPrice = GetTicketPrice(isExistsBaggage, route.Id, departurePointId, arrivalPointId, amountOfTea, amountOfLinens, type, wagonType);
            Station departurePoint = UnitOfWork.Stations.Get(departurePointId);
            Station arrivalPoint = UnitOfWork.Stations.Get(arrivalPointId);
            if (departurePoint == null || arrivalPoint == null)
            {
                throw new EntityNotFoundException(typeof(Station));
            }

            List<TrainStop> stops = route.Stops.ToList();
            TrainStop firstTrainStop = stops.First(f => f.Station.Id == departurePoint.Id);
            TrainStop secondTrainStop = stops.First(f => f.Station.Id == arrivalPoint.Id);

            if (stops.IndexOf(firstTrainStop) >= stops.IndexOf(secondTrainStop))
            {
                throw new ArrivalPointBeforeDepartureException();
            }

            var fiscalReceiptNumber = "123";
            var ticketAttributes = new TicketAttributes(type, studentNumber, isExistsBaggage, age, amountOfTea, amountOfLinens, ticketPrice);
            var ticket = new BoughtTicket(
                client, 
                place, 
                departurePoint.FullName, 
                arrivalPoint.FullName, 
                fiscalReceiptNumber, 
                DateTime.UtcNow, 
                ticketAttributes);

            place.State = Place.PlaceState.Busy;
            UnitOfWork.Places.Update(place);
            UnitOfWork.BoughtTickets.Create(ticket);
            UnitOfWork.Save();

            return ticket.Id;
        }

        public TicketDto Get(int id)
        {
            BoughtTicket ticket = UnitOfWork.BoughtTickets.Get(id);
            if (ticket == null)
            {
                throw new EntityNotFoundException(typeof(Ticket));
            }

            return ticket.ToDto();
        }

        public float GetTicketPrice(
            int placeId,
            int departurePointId,
            int arrivalPointId,
            int amountOfTea,
            int amountOfLinens,
            bool isExistsBaggage,
            TicketType ticketType)
        {
            Place place = UnitOfWork.Places.Get(placeId);
            if (place == null)
            {
                throw new EntityNotFoundException(typeof(Place));
            }

            Train train = place.Wagon.Train;
            if (train == null)
            {
                throw new EntityNotFoundException(typeof(Train));
            }

            Flight flight = place.Wagon.Train.Flight;
            if (flight == null)
            {
                throw new EntityNotOnFlightException(typeof(Flight));
            }

            return GetTicketPrice(
                isExistsBaggage,
                flight.Route.Id,
                departurePointId,
                arrivalPointId, 
                amountOfTea,
                amountOfLinens,
                ticketType,
                place.Wagon.Type);
        }

        private float GetTicketPrice(
            bool isExistsBaggage, 
            int routeId,
            int departurePointId,
            int arrivalPointId, 
            int amountOfTea,
            int amountOfLinens, 
            TicketType ticketType, 
            WagonType wagonType)
        {
            var totalPrice = 0f;
            if (isExistsBaggage)
            {
                totalPrice += PriceService.Get("Baggage").Value;
            }

            double routeDistance = RouteService.GetRouteDistance(routeId, departurePointId, arrivalPointId);
            totalPrice += (float)(routeDistance * PriceService.Get("PriceForOneKilometer").Value);
            totalPrice += amountOfTea * PriceService.Get("Tea").Value;
            totalPrice += amountOfLinens * PriceService.Get("Linen").Value;

            if (ticketType == TicketType.Child)
            {
                totalPrice *= 1 - PriceService.Get("ChildDiscountInPercent").Value;
            }

            if (ticketType == TicketType.Student)
            {
                totalPrice *= 1 - PriceService.Get("StudentDiscountInPercent").Value;
            }

            switch (wagonType)
            {
                case WagonType.SittingSecondClass:
                    totalPrice *= PriceService.Get("PriceСoefficientForSittingSecondClass").Value;
                    break;
                case WagonType.SittingFirstClass:
                    totalPrice *= PriceService.Get("PriceСoefficientForSittingFirstClass").Value;
                    break;
                case WagonType.ReservedSeat:
                    totalPrice *= PriceService.Get("PriceСoefficientForReservedSeat").Value;
                    break;
                case WagonType.Compartment:
                    totalPrice *= PriceService.Get("PriceСoefficientForCompartment").Value;
                    break;
                case WagonType.Lux:
                    totalPrice *= PriceService.Get("PriceСoefficientForLux").Value;
                    break;
            }

            return (float)Math.Round(totalPrice, 2);
        }
    }
}
