﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using Railway.Exceptions.Service;
using Railway.Services.Dto;
using Railway.Services.Dto.Utils;
using Railway.Services.Interfaces;

namespace Railway.Infrastructure.Services
{
    public class FlightService : IFlightService
    {
        [Inject]
        public IRouteService RouteService { private get; set; }

        [Inject]
        public IUnitOfWork UnitOfWork { private get; set; }

        public FlightDto GetFlightForPlace(int placeId)
        {
            Place place = UnitOfWork.Places.Get(placeId);
            if (place == null)
            {
                throw new EntityNotFoundException(typeof(Place));
            }

            Train train = place.Wagon.Train;
            if (train == null)
            {
                throw new EntityNotFoundException(typeof(Train));
            }

            Flight flight = place.Wagon.Train.Flight;
            if (flight == null)
            {
                throw new EntityNotOnFlightException(typeof(Flight));
            }

            return flight.ToDto();
        }

        public int Create(DateTime departureDate, int trainId, int routeId, int amountOfLinens, int amountOfTea)
        {
            Train train = UnitOfWork.Trains.Get(trainId);
            if (train == null)
            {
                throw new EntityNotFoundException(typeof(Train));
            }

            Route route = GetNotNullRoute(routeId);
            DateTime arrivalDate = departureDate + GetFlightDuration(train.Speed, route);
            Flight flight = new Flight(Flight.FlightState.Ready, departureDate, arrivalDate, train, route, amountOfLinens, amountOfTea);
            UnitOfWork.Flights.Create(flight);
            UnitOfWork.Save();

            return flight.Id;
        }

        public FlightDto Get(int id)
        {
            Flight flight = GetNotNullFlight(id);

            return flight.ToDto();
        }

        public IEnumerable<FlightDto> GetFlightsForRoute(int routeId)
        {
            Route route = GetNotNullRoute(routeId);
            IEnumerable<Flight> flights = route.Flights.ToList()
                                                       .Where(f => f.State == Flight.FlightState.Ready && 
                                                                   f.DepartureDate > DateTime.UtcNow);

            return flights.Select(f => f.ToDto());
        }

        public IEnumerable<WagonDto> GetWagonsForFlight(int flightId)
        {
            Flight flight = GetNotNullFlight(flightId);

            return flight.Train.Wagons.Select(w => w.ToDto());
        }

        public IEnumerable<int> GetAllIds()
        {
            return UnitOfWork.Flights.Select(f => f.Id);
        }

        private TimeSpan GetFlightDuration(int trainSpeed, Route route)
        {
            var duration = new TimeSpan();
            foreach (var stop in route.Stops)
            {
                duration += stop.Time;
            }

            double distance = RouteService.GetRouteDistance(route.Id);
            double time = distance / trainSpeed;
            var hours = (int)time;
            var minutes = (int)((time - hours) * 60);
            duration += new TimeSpan(hours, minutes, 0);

            return duration;
        }

        private Flight GetNotNullFlight(int id)
        {
            Flight flight = UnitOfWork.Flights.Get(id);
            if (flight == null)
            {
                throw new EntityNotFoundException(typeof(Flight));
            }

            return flight;
        }

        private Route GetNotNullRoute(int id)
        {
            Route route = UnitOfWork.Routes.Get(id);
            if (route == null)
            {
                throw new EntityNotFoundException(typeof(Route));
            }

            return route;
        }
    }
}
