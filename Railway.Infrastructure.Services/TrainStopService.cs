﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using Railway.Exceptions.Service;
using Railway.Services.Dto;
using Railway.Services.Dto.Utils;
using Railway.Services.Interfaces;
using Railway.Services.Interfaces.Utils;

namespace Railway.Infrastructure.Services
{
    public class TrainStopService : ITrainStopService
    {
        [Inject]
        public IUnitOfWork UnitOfWork { private get; set; }

        [Inject]
        public RouteService RouteService { private get; set; }

        public int Create(int stationId, TimeSpan duration)
        {
            Station station = UnitOfWork.Stations.Get(stationId);
            var trainStop = new TrainStop(station, duration);
            UnitOfWork.TrainStops.Create(trainStop);
            UnitOfWork.Save();

            return trainStop.Id;
        }

        public TrainStopDto Get(int id)
        {
            TrainStop trainStop = GetNotNullTrainStop(id);

            return trainStop.ToDto();
        }

        public IDictionary<string, TimeSpan> GetStopsForRoute(int routeId)
        {
            Route route = UnitOfWork.Routes.Get(routeId);
            if (route == null)
            {
                throw new EntityNotFoundException(typeof(Route));
            }

            var result = new Dictionary<string, TimeSpan>();
            route.Stops.ToList().ForEach(s => result.Add(s.Station.FullName, s.Time));

            return result;
        }

        public IEnumerable<int> GetAllIds()
        {
            return UnitOfWork.TrainStops.Select(ts => ts.Id);
        }

        public IEnumerable<TrainStopInformation> GetStopsForFlight(int flightId)
        {
            Flight flight = UnitOfWork.Flights.Get(flightId);
            if (flight == null)
            {
                throw new EntityNotFoundException(typeof(Flight));
            }

            IList<TrainStop> stops = flight.Route.Stops.ToList();
            var result = new List<TrainStopInformation>();

            foreach (TrainStop t in stops)
            {
                DateTime arrivalDate = GetArrivalDate(flight.DepartureDate, stops, t.Id, flight.Route.Id, flight.Train.Speed);
                result.Add(new TrainStopInformation
                {
                    Station = t.Station.FullName,
                    ArrivalDate = arrivalDate,
                    DepartureDate = arrivalDate + t.Time
                });
            }

            return result;
        }

        private DateTime GetArrivalDate(DateTime departureDate, IList<TrainStop> stops, int stopId, int routeId, int trainSpeed)
        {
            DateTime arrivalDate = departureDate;

            for (int i = 0; i < stops.Count; i++)
            {
                if (stops[i].Id == stopId)
                {
                    return arrivalDate;
                }

                arrivalDate += stops[i].Time;

                if (i < (stops.Count - 1))
                {
                    int currentStationId = stops[i].Station.Id;
                    int nextStationId = stops[i + 1].Station.Id;
                    double distance = RouteService.GetRouteDistance(routeId, currentStationId, nextStationId);
                    double hours = distance / trainSpeed;
                    int fullHours = (int)hours;
                    int minutes = (int)((hours - fullHours) * 60);

                    arrivalDate += new TimeSpan(fullHours, minutes, 0);
                }
            }

            return arrivalDate;
        }

        private TrainStop GetNotNullTrainStop(int id)
        {
            TrainStop trainStop = UnitOfWork.TrainStops.Get(id);
            if (trainStop == null)
            {
                throw new EntityNotFoundException(typeof(TrainStop));
            }

            return trainStop;
        }
    }
}
