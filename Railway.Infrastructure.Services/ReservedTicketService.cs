﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using Railway.Exceptions.Service;
using Railway.Services.Dto;
using Railway.Services.Dto.Utils;
using Railway.Services.Interfaces;

namespace Railway.Infrastructure.Services
{
    public class ReservedTicketService : IReservedTicketService
    {
        [Inject]
        public IUnitOfWork UnitOfWork { private get; set; }

        public IEnumerable<int> GetAllIds()
        {
            return UnitOfWork.ReservedTickets
                .GetAll()
                .ToList()
                .Where(t => t.GetType() == typeof(ReservedTicket))
                .Select(t => t.Id);
        }

        public TicketDto Get(int id)
        {
            Ticket ticket = GetNotNullReservedTicket(id);

            return ticket.ToDto();
        }

        public int ReserveTicket(int clientId, int placeId, int departurePointId, int arrivalPointId)
        {
            Client client = UnitOfWork.Clients.Get(clientId);
            if (client == null)
            {
                throw new EntityNotFoundException(typeof(Client));
            }

            Place place = UnitOfWork.Places.Get(placeId);
            if (place == null)
            {
                throw new EntityNotFoundException(typeof(Place));
            }

            Station departurePoint = UnitOfWork.Stations.Get(departurePointId);
            Station arrivalPoint = UnitOfWork.Stations.Get(arrivalPointId);
            if (departurePoint == null || arrivalPoint == null)
            {
                throw new EntityNotFoundException(typeof(Station));
            }

            var ticket = new ReservedTicket(client, place, departurePoint.FullName, arrivalPoint.FullName, DateTime.Now);
            place.State = Place.PlaceState.Reserved;

            UnitOfWork.Places.Update(place);
            UnitOfWork.ReservedTickets.Create(ticket);
            UnitOfWork.Save();

            return ticket.Id;
        }

        private ReservedTicket GetNotNullReservedTicket(int id)
        {
            ReservedTicket ticket = UnitOfWork.ReservedTickets.Get(id);
            if (ticket == null)
            {
                throw new EntityNotFoundException(typeof(ReservedTicket));
            }

            return ticket;
        }
    }
}
