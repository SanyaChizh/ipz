﻿using System;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;

namespace Railway.Infrastructure.EFDataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _dbContext;
        private readonly Lazy<Repository<BoughtTicket, int>> _boughtTicketRepository;
        private readonly Lazy<Repository<Client, int>> _clientRepository;
        private readonly Lazy<Repository<Driver, int>> _driverRepository;
        private readonly Lazy<Repository<Flight, int>> _flightRepository;
        private readonly Lazy<Repository<Place, int>> _placeRepository;
        private readonly Lazy<Repository<PriceData, int>> _priceDataRepository;
        private readonly Lazy<Repository<RedeemedTicket, int>> _redeemedTicketRepository;
        private readonly Lazy<Repository<ReservedTicket, int>> _reservedTicketRepository;
        private readonly Lazy<Repository<Route, int>> _routeRepository;
        private readonly Lazy<Repository<Station, int>> _stationRepository;
        private readonly Lazy<Repository<StationsDistance, int>> _stationsDistanceRepository;
        private readonly Lazy<Repository<Train, int>> _trainRepository;
        private readonly Lazy<Repository<TrainStop, int>> _trainStopRepository;
        private readonly Lazy<Repository<Wagon, int>> _wagonRepository;
        private readonly Lazy<Repository<ShoppingCart, int>> _shoppingCartRepository;

        private bool _disposed;

        public UnitOfWork()
        {
            _dbContext = new ApplicationContext();

            _boughtTicketRepository = InitRepository<BoughtTicket>(_dbContext);
            _clientRepository = InitRepository<Client>(_dbContext);
            _driverRepository = InitRepository<Driver>(_dbContext);
            _flightRepository = InitRepository<Flight>(_dbContext);
            _placeRepository = InitRepository<Place>(_dbContext);
            _priceDataRepository = InitRepository<PriceData>(_dbContext);
            _redeemedTicketRepository = InitRepository<RedeemedTicket>(_dbContext);
            _reservedTicketRepository = InitRepository<ReservedTicket>(_dbContext);
            _routeRepository = InitRepository<Route>(_dbContext);
            _stationRepository = InitRepository<Station>(_dbContext);
            _stationsDistanceRepository = InitRepository<StationsDistance>(_dbContext);
            _trainRepository = InitRepository<Train>(_dbContext);
            _trainStopRepository = InitRepository<TrainStop>(_dbContext);
            _wagonRepository = InitRepository<Wagon>(_dbContext);
            _shoppingCartRepository = InitRepository<ShoppingCart>(_dbContext);
        }

        public IRepository<BoughtTicket, int> BoughtTickets => _boughtTicketRepository.Value;

        public IRepository<Client, int> Clients => _clientRepository.Value;

        public IRepository<Driver, int> Drivers => _driverRepository.Value;

        public IRepository<Flight, int> Flights => _flightRepository.Value;

        public IRepository<Place, int> Places => _placeRepository.Value;

        public IRepository<PriceData, int> PricesData => _priceDataRepository.Value;

        public IRepository<RedeemedTicket, int> RedeemedTickets => _redeemedTicketRepository.Value;

        public IRepository<ReservedTicket, int> ReservedTickets => _reservedTicketRepository.Value;

        public IRepository<Route, int> Routes => _routeRepository.Value;

        public IRepository<Station, int> Stations => _stationRepository.Value;

        public IRepository<StationsDistance, int> StationsDistances => _stationsDistanceRepository.Value;

        public IRepository<Train, int> Trains => _trainRepository.Value;

        public IRepository<TrainStop, int> TrainStops => _trainStopRepository.Value;

        public IRepository<Wagon, int> Wagons => _wagonRepository.Value;

        public IRepository<ShoppingCart, int> ShoppingCarts => _shoppingCartRepository.Value;

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _dbContext.Dispose();
            }

            _disposed = true;
        }

        private static Lazy<Repository<T, int>> InitRepository<T>(ApplicationContext context) where T : class
        {
            return new Lazy<Repository<T, int>>(() => new Repository<T, int>(context));
        }
    }
}
