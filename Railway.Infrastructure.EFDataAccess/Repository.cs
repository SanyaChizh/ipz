﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Railway.Domain.Interfaces;

namespace Railway.Infrastructure.EFDataAccess
{
    public class Repository<T, TKey> : IRepository<T, TKey>
        where T : class
    {
        private readonly ApplicationContext _dbContext;

        public Repository(ApplicationContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _dbContext.Set<T>().ToList();
        }

        public virtual T Get(TKey id)
        {
            return _dbContext.Set<T>().Find(id);
        }

        public virtual IEnumerable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return _dbContext.Set<T>().Where(predicate).ToList();
        }

        public virtual IEnumerable<TResult> Select<TResult>(Expression<Func<T, TResult>> selector)
        {
            return _dbContext.Set<T>().Select(selector).ToList();
        }

        public virtual void Create(T item)
        {
            _dbContext.Set<T>().Add(item);
        }

        public virtual void Update(T item)
        {
            _dbContext.Entry(item).State = EntityState.Modified;
        }

        public virtual void Delete(T item)
        {
            _dbContext.Set<T>().Remove(item);
        }
    }
}
