﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using Railway.Domain.Core.Models;
using Railway.Infrastructure.EFDataAccess.Configurations;

namespace Railway.Infrastructure.EFDataAccess
{
    public class ApplicationContext : DbContext
    {
        public DbSet<BoughtTicket> BoughtTickets { get; set; }

        public DbSet<Client> Clients { get; set; }

        public DbSet<Driver> Drivers { get; set; }

        public DbSet<Flight> Flights { get; set; }

        public DbSet<Place> Places { get; set; }

        public DbSet<PriceData> PricesData { get; set; }

        public DbSet<RedeemedTicket> RedeemedTickets { get; set; }

        public DbSet<ReservedTicket> ReservedTickets { get; set; }

        public DbSet<Route> Routes { get; set; }

        public DbSet<Station> Stations { get; set; }

        public DbSet<StationsDistance> StationsDistances { get; set; }

        public DbSet<Train> Trains { get; set; }

        public DbSet<TrainStop> TrainStops { get; set; }

        public DbSet<Wagon> Wagons { get; set; }

        public DbSet<ShoppingCart> ShopppingCarts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ComplexType<TicketAttributes>();

            modelBuilder.Entity<Station>().Ignore(s => s.FullName);
            modelBuilder.Entity<Route>().Ignore(r => r.DeparturePoint);
            modelBuilder.Entity<Route>().Ignore(r => r.ArrivalPoint);

            modelBuilder.Entity<PriceData>()
                .Property(g => g.Name)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("UniquePriceDataName", 1) { IsUnique = true }));

            modelBuilder.Configurations.Add(new BoughtTicketConfiguration<BoughtTicket>());
            modelBuilder.Configurations.Add(new ClientConfiguration());
            modelBuilder.Configurations.Add(new DriverConfiguration());
            modelBuilder.Configurations.Add(new FlightConfiguration());
            modelBuilder.Configurations.Add(new PlaceConfiguration());
            modelBuilder.Configurations.Add(new PriceDataConfiguration());
            modelBuilder.Configurations.Add(new RedeemedTicketConfiguration());
            modelBuilder.Configurations.Add(new RouteConfiguration());
            modelBuilder.Configurations.Add(new StationConfiguration());
            modelBuilder.Configurations.Add(new StationsDistanceConfiguration());
            modelBuilder.Configurations.Add(new TrainConfiguration());
            modelBuilder.Configurations.Add(new TrainStopConfiguration());
            modelBuilder.Configurations.Add(new WagonConfiguration());
            modelBuilder.Configurations.Add(new ShoppingCartConfiguration());         
        }
    }
}
