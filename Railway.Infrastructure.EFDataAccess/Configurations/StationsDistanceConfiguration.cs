﻿using System.Data.Entity.ModelConfiguration;
using Railway.Domain.Core.Models;

namespace Railway.Infrastructure.EFDataAccess.Configurations
{
    public class StationsDistanceConfiguration : EntityTypeConfiguration<StationsDistance>
    {
        public StationsDistanceConfiguration()
        {
            HasMany(sd => sd.Stations).WithMany();
        }
    }
}
