﻿using System.Data.Entity.ModelConfiguration;
using Railway.Domain.Core.Models;

namespace Railway.Infrastructure.EFDataAccess.Configurations
{
    public class ShoppingCartConfiguration : EntityTypeConfiguration<ShoppingCart>
    {
        public ShoppingCartConfiguration()
        {
            HasMany(sc => sc.Tickets);
        }
    }
}
