﻿using System.Data.Entity.ModelConfiguration;
using Railway.Domain.Core.Models;

namespace Railway.Infrastructure.EFDataAccess.Configurations
{
    public class TrainStopConfiguration : EntityTypeConfiguration<TrainStop>
    {
        public TrainStopConfiguration()
        {
            HasRequired(s => s.Station).WithMany();
        }
    }
}
