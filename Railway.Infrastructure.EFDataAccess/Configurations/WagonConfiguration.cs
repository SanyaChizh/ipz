﻿using System.Data.Entity.ModelConfiguration;
using Railway.Domain.Core.Models;

namespace Railway.Infrastructure.EFDataAccess.Configurations
{
    public class WagonConfiguration : EntityTypeConfiguration<Wagon>
    {
        public WagonConfiguration()
        {
            HasOptional(w => w.Train).WithMany(t => t.Wagons);
            HasOptional(w => w.Station).WithMany(s => s.Wagons);
        }
    }
}
