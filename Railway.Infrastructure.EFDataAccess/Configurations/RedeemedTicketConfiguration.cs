﻿using Railway.Domain.Core.Models;

namespace Railway.Infrastructure.EFDataAccess.Configurations
{
    public class RedeemedTicketConfiguration : TicketConfiguration<RedeemedTicket>
    {
        public RedeemedTicketConfiguration()
        {
            Property(t => t.FiscalReceiptNumber).IsRequired();
        }
    }
}
