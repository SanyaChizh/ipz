﻿using System.Data.Entity.ModelConfiguration;
using Railway.Domain.Core.Models;

namespace Railway.Infrastructure.EFDataAccess.Configurations
{
    public abstract class TicketConfiguration<T> : EntityTypeConfiguration<T>
        where T : Ticket
    {
        protected TicketConfiguration()
        {
            HasRequired(t => t.Client).WithMany();
            HasRequired(t => t.Place).WithOptional();
        }
    }
}
