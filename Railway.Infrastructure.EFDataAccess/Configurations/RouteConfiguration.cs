﻿using System.Data.Entity.ModelConfiguration;
using Railway.Domain.Core.Models;

namespace Railway.Infrastructure.EFDataAccess.Configurations
{
    public class RouteConfiguration : EntityTypeConfiguration<Route>
    {
        public RouteConfiguration()
        {
            HasMany(r => r.Stops).WithOptional();
        }
    }
}
