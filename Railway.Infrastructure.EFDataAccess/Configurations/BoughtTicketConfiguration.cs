﻿using Railway.Domain.Core.Models;

namespace Railway.Infrastructure.EFDataAccess.Configurations
{
    public class BoughtTicketConfiguration<T> : TicketConfiguration<T>
        where T : BoughtTicket
    {
        public BoughtTicketConfiguration()
        {
            Property(t => t.FiscalReceiptNumber).IsRequired();
        }
    }
}
