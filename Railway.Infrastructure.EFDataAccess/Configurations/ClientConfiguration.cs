﻿using System.Data.Entity.ModelConfiguration;
using Railway.Domain.Core.Models;

namespace Railway.Infrastructure.EFDataAccess.Configurations
{
    public class ClientConfiguration : EntityTypeConfiguration<Client>
    {
        public ClientConfiguration()
        {
            Property(c => c.FirstName).IsRequired();
            Property(c => c.LastName).IsRequired();
            Property(c => c.Email).IsRequired();
        }
    }
}
