﻿using System.Data.Entity.ModelConfiguration;
using Railway.Domain.Core.Models;

namespace Railway.Infrastructure.EFDataAccess.Configurations
{
    public class DriverConfiguration : EntityTypeConfiguration<Driver>
    {
        public DriverConfiguration()
        {
            Property(d => d.FirstName).IsRequired();
            Property(d => d.LastName).IsRequired();
            HasOptional(d => d.Train).WithRequired(t => t.Driver);
            HasOptional(d => d.Station).WithMany(s => s.Drivers);
        }
    }
}
