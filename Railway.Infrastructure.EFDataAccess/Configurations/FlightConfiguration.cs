﻿using System.Data.Entity.ModelConfiguration;
using Railway.Domain.Core.Models;

namespace Railway.Infrastructure.EFDataAccess.Configurations
{
    public class FlightConfiguration : EntityTypeConfiguration<Flight>
    {
        public FlightConfiguration()
        {
            HasRequired(f => f.Train).WithOptional(f => f.Flight).WillCascadeOnDelete(true);
            HasRequired(f => f.Route).WithMany(r => r.Flights).WillCascadeOnDelete(true);
        }
    }
}
