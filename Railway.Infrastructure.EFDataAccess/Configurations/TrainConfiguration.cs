﻿using System.Data.Entity.ModelConfiguration;
using Railway.Domain.Core.Models;

namespace Railway.Infrastructure.EFDataAccess.Configurations
{
    public class TrainConfiguration : EntityTypeConfiguration<Train>
    {
        public TrainConfiguration()
        {
            HasOptional(t => t.Station).WithMany(s => s.Trains);
        }
    }
}
