﻿using System.Data.Entity.ModelConfiguration;
using Railway.Domain.Core.Models;

namespace Railway.Infrastructure.EFDataAccess.Configurations
{
    public class PriceDataConfiguration : EntityTypeConfiguration<PriceData>
    {
        public PriceDataConfiguration()
        {
            Property(d => d.Name).IsRequired();
        }
    }
}
