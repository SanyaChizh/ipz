﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using Railway.Services.Dto;

namespace Railway.Infrastructure.Services.Tests
{
    [TestFixture]
    public class FlightServiceTest
    {
        private FlightService _flightService;

        private Mock<IUnitOfWork> _unitOfWork;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();

            _flightService = new FlightService { UnitOfWork = _unitOfWork.Object };
        }

        [Test]
        public void ShouldGetFlightForPlace()
        {
            const int amountOfTea = 100;

            var place = new Place
            {
                Id = 1,
                State = Place.PlaceState.Free,
                Wagon = new Wagon
                {
                    Id = 1,
                    AmountOfPlaces = 40,
                    Places = new List<Place>(),
                    Station = new Station
                    {
                        City = "City",
                        Drivers = new List<Driver>
                        {
                            new Driver("firstName", "lastName", new Train(), new Station())
                        },
                        Name = "Name",
                        Id = 1,
                        Trains = new List<Train>(),
                        Wagons = new List<Wagon>()
                    },
                    Train = new Train
                    {
                        Speed = 120,
                        Id = 1,
                        Flight = new Flight
                        {
                            AmountOfTea = amountOfTea,
                            AmountOfLinens = 100,
                            ArrivalDate = DateTime.UtcNow.AddMonths(1),
                            DepartureDate = DateTime.UtcNow.AddMonths(1).AddDays(-1),
                            Id = 1,
                            State = Flight.FlightState.Ready,
                            Route = new Route() { Id = 1 },
                            Train = new Train() { Id = 1 }
                        }
                    }
                }
            };

            _unitOfWork
                .Setup(m => m.Places.Get(It.IsAny<int>()))
                .Returns(place);

            FlightDto result = _flightService.GetFlightForPlace(It.IsAny<int>());

            _unitOfWork.Verify(m => m.Places.Get(It.IsAny<int>()), Times.AtLeastOnce);
            Assert.AreEqual(amountOfTea, result.AmountOfTea);
        }
    }
}
