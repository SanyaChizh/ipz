﻿using System;
using Moq;
using NUnit.Framework;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace Railway.Infrastructure.Services.Tests
{
    [TestFixture]
    public class DriverServiceTest
    {
        private Mock<IUnitOfWork> _unitOfWorkMock;
        private DriverService _driverService;

        [SetUp]
        public void SetUp()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _driverService = new DriverService { UnitOfWork = _unitOfWorkMock.Object };
        }

        [Test]
        public void ShouldCreateDriver()
        {
            _unitOfWorkMock.Setup(
                m => m.Stations.Get(It.IsAny<int>()))
                .Returns(new Station());

            _unitOfWorkMock.Setup(
                m => m.Drivers.Create(It.IsAny<Driver>()));

            _unitOfWorkMock.Setup(
                m => m.Save());

            _driverService.Create("Sasha", "Chizh", 1);

            _unitOfWorkMock.Verify(m => m.Save());
        }

        [Test]
        public void ShouldReturnAllDriversIds()
        {
            _unitOfWorkMock
                .Setup(m => m.Drivers.Select((It.IsAny<Expression<Func<Driver, int>>>())))
                .Returns(new List<int>());

            _driverService.GetAllIds();

            _unitOfWorkMock.Verify(m => m.Drivers.Select(It.IsAny<Expression<Func<Driver, int>>>()));
        }

    }
}
