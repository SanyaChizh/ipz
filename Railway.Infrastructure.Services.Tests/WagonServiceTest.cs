﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Moq;
using NUnit.Framework;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using Railway.Exceptions.Service;
using Railway.Services.Dto;

namespace Railway.Infrastructure.Services.Tests
{
    [TestFixture]
    public class WagonServiceTest
    {
        private WagonService _wagonService;
        private Mock<IUnitOfWork> _unitOfWork;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();

            _wagonService = new WagonService { UnitOfWork = _unitOfWork.Object };
        }

        [Test]
        public void ShouldReturnAllWagonsIds()
        {
            _unitOfWork
                .Setup(m => m.Wagons.Select(It.IsAny<Expression<Func<Wagon, int>>>()))
                .Returns(new List<int>());

            _wagonService.GetAllIds();

            _unitOfWork.Verify(m => m.Wagons.Select(It.IsAny<Expression<Func<Wagon, int>>>()));
        }

        [Test]
        public void ShouldReturnPlacesForWagon()
        {
            _unitOfWork
                .Setup(m => m.Wagons.Get(It.IsAny<int>()))
                .Returns(new Wagon { Places = new List<Place> { new Place(Place.PlaceState.Free, new Wagon()) } });

            var result = _wagonService.GetPlacesForWagon(It.IsAny<int>());

            _unitOfWork.Verify(m => m.Wagons.Get(It.IsAny<int>()), Times.AtLeastOnce);
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void ShouldThrowException_ForCreatingWagon_AtNotExistenStation()
        {
            _unitOfWork
                .Setup(s => s.Stations.Get(It.IsAny<int>()))
                .Returns((Station)null);

            Assert.That(
                () => _wagonService.Create(Wagon.WagonType.Lux, stationId: 1, amountOfPlaces: 40), 
                Throws.TypeOf<EntityNotFoundException>());
        }

        [Test]
        public void ShouldCreateWagon()
        {
            _unitOfWork
                .Setup(m => m.Stations.Get(It.IsAny<int>()))
                .Returns(new Station());

            _unitOfWork
                .Setup(m => m.Wagons.Create(It.IsAny<Wagon>()));

            _unitOfWork.Setup(m => m.Save());

            _wagonService.Create(Wagon.WagonType.Lux, stationId: 1, amountOfPlaces: 1);

            _unitOfWork.Verify(m => m.Stations.Get(It.IsAny<int>()), Times.AtLeastOnce);
            _unitOfWork.Verify(m => m.Wagons.Create(It.IsAny<Wagon>()), Times.AtLeastOnce);
            _unitOfWork.Verify(m => m.Save(), Times.AtLeastOnce);
        }

        [Test]
        public void ShouldThrowException_WhenGet_NotExisten_Wagon()
        {
            _unitOfWork
                .Setup(m => m.Wagons.Get(It.IsAny<int>()))
                .Returns((Wagon)null);

            Assert.That(() => _wagonService.Get(It.IsAny<int>()), Throws.TypeOf<EntityNotFoundException>());
        }

        [Test]
        public void ShouldReturnWagon()
        {
            const int amountOfPlaces = 40;

            _unitOfWork
                .Setup(m => m.Wagons.Get(It.IsAny<int>()))
                .Returns(new Wagon(Wagon.WagonType.Lux, new Train(), new Station(), amountOfPlaces));

            WagonDto result = _wagonService.Get(It.IsAny<int>());

            _unitOfWork.Verify(m => m.Wagons.Get(It.IsAny<int>()), Times.AtLeastOnce);
            Assert.AreEqual(amountOfPlaces, result.AmountOfPlaces);
        }
    }
}
