﻿using System;
using Moq;
using NUnit.Framework;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace Railway.Infrastructure.Services.Tests
{
    [TestFixture]
    public class ShoppingCartServiceTest
    {
        private Mock<IUnitOfWork> _unitOfWorkMock;
        private ShoppingCartService _shoppingCartService;

        [SetUp]
        public void SetUp()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _shoppingCartService = new ShoppingCartService();
            _shoppingCartService.UnitOfWork = _unitOfWorkMock.Object;
        }
        [Test]
        public void ShouldCreateShoppingCart()
        {
            _unitOfWorkMock.Setup(
                m => m.ShoppingCarts.Get(It.IsAny<int>()))
                .Returns(new ShoppingCart());

            _unitOfWorkMock.Setup(
                m => m.ShoppingCarts.Create(It.IsAny<ShoppingCart>()));

            _unitOfWorkMock.Setup(
                m => m.Save());

            _shoppingCartService.Create();

            _unitOfWorkMock.Verify(m => m.Save());
        }

        [Test]
        public void ShouldReturnAllShoppingCartIds()
        {
            _unitOfWorkMock
                .Setup(m => m.ShoppingCarts.Select((It.IsAny<Expression<Func<ShoppingCart, int>>>())))
                .Returns(new List<int>());

            _shoppingCartService.GetAllIds();

            _unitOfWorkMock.Verify(m => m.ShoppingCarts.Select(It.IsAny<Expression<Func<ShoppingCart, int>>>()));
        }
    }
}
