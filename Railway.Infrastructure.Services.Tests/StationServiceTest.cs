﻿using System;
using Moq;
using NUnit.Framework;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace Railway.Infrastructure.Services.Tests
{
    [TestFixture]
    public class StationServiceTest
    {
        private Mock<IUnitOfWork> _unitOfWorkMock;
        private StationService _stationService;

        [SetUp]
        public void SetUp()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _stationService = new StationService();
            _stationService.UnitOfWork = _unitOfWorkMock.Object;
        }

        [Test]
        public void ShouldCreateStation()
        {
            _unitOfWorkMock.Setup(
                m => m.Stations.Get(It.IsAny<int>()))
                .Returns(new Station());

            _unitOfWorkMock.Setup(
                m => m.Stations.Create(It.IsAny<Station>()));

            _unitOfWorkMock.Setup(
                m => m.Save());

            _stationService.Create("Kyiv-pas", "Kyiv");

            _unitOfWorkMock.Verify(m => m.Save());
        }

        [Test]
        public void ShouldReturnAllStationsIds()
        {
            _unitOfWorkMock
                .Setup(m => m.Stations.Select((It.IsAny<Expression<Func<Station, int>>>())))
                .Returns(new List<int>());

            _stationService.GetAllIds();

            _unitOfWorkMock.Verify(m => m.Stations.Select(It.IsAny<Expression<Func<Station, int>>>()));
        }
    }
}
