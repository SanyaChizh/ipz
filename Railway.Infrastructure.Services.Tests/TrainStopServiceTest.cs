﻿using System;
using Moq;
using NUnit.Framework;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace Railway.Infrastructure.Services.Tests
{
    [TestFixture]
    public class TrainStopServiceTest
    {
        private Mock<IUnitOfWork> _unitOfWorkMock;
        private TrainStopService _trainStopService;

        [SetUp]
        public void SetUp()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _trainStopService = new TrainStopService();
            _trainStopService.UnitOfWork = _unitOfWorkMock.Object;
        }

        [Test]
        public void ShouldCreateTrainStop()
        {
            _unitOfWorkMock.Setup(
                m => m.Stations.Get(It.IsAny<int>()))
                .Returns(new Station());

            _unitOfWorkMock.Setup(
                m => m.TrainStops.Create(It.IsAny<TrainStop>()));

            _unitOfWorkMock.Setup(
                m => m.Save());

            _trainStopService.Create(1, TimeSpan.MinValue);

            _unitOfWorkMock.Verify(m => m.Save(), Times.AtLeastOnce);
        }

        [Test]
        public void ShouldReturnAllTrainStopsIds()
        {
            _unitOfWorkMock
                .Setup(m => m.TrainStops.Select((It.IsAny<Expression<Func<TrainStop, int>>>())))
                .Returns(new List<int>());

            _trainStopService.GetAllIds();

            _unitOfWorkMock.Verify(m => m.TrainStops.Select(It.IsAny<Expression<Func<TrainStop, int>>>()));
        }

       

    }
}
