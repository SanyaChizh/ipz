﻿using System;
using Moq;
using NUnit.Framework;
using Railway.Domain.Core.Models;
using Railway.Domain.Interfaces;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace Railway.Infrastructure.Services.Tests
{
    [TestFixture]
    public class PriceServiceTest
    {
        private Mock<IUnitOfWork> _unitOfWorkMock;
        private PriceService _priceService;

        [SetUp]
        public void SetUp()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _priceService = new PriceService();
            _priceService.UnitOfWork = _unitOfWorkMock.Object;
        }

        [Test]
        public void ShouldCreatePrice()
        {
            _unitOfWorkMock.Setup(
                    m => m.PricesData.Get(It.IsAny<int>()))
                .Returns(new PriceData());

            _unitOfWorkMock.Setup(
                m => m.PricesData.Create(It.IsAny<PriceData>()));

            _unitOfWorkMock.Setup(
                m => m.Save());

            _priceService.Create("Name", 1000);

            _unitOfWorkMock.Verify(m => m.Save());
        }

        [Test]
        public void ShouldReturnAllPricesIds()
        {
            _unitOfWorkMock
                .Setup(m => m.PricesData.Select((It.IsAny<Expression<Func<PriceData, int>>>())))
                .Returns(new List<int>());

            _priceService.GetAllIds();

            _unitOfWorkMock.Verify(m => m.PricesData.Select(It.IsAny<Expression<Func<PriceData, int>>>()));
        }
    }
}
