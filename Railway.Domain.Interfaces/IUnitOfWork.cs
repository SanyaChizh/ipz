﻿using System;
using Railway.Domain.Core.Models;

namespace Railway.Domain.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<BoughtTicket, int> BoughtTickets { get; }

        IRepository<Client, int> Clients { get; }

        IRepository<Driver, int> Drivers { get; }

        IRepository<Flight, int> Flights { get; }

        IRepository<Place, int> Places { get; }

        IRepository<PriceData, int> PricesData { get; }

        IRepository<RedeemedTicket, int> RedeemedTickets { get; }

        IRepository<ReservedTicket, int> ReservedTickets { get; }

        IRepository<Route, int> Routes { get; }

        IRepository<Station, int> Stations { get; }

        IRepository<StationsDistance, int> StationsDistances { get; }

        IRepository<Train, int> Trains { get; }

        IRepository<TrainStop, int> TrainStops { get; }

        IRepository<Wagon, int> Wagons { get; }

        IRepository<ShoppingCart, int> ShoppingCarts { get; }

        void Save();
    }
}
