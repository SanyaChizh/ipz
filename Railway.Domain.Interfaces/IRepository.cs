﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Railway.Domain.Interfaces
{
    public interface IRepository<T, in TKey> where T : class
    {
        IEnumerable<T> GetAll();

        T Get(TKey id);

        IEnumerable<T> Find(Expression<Func<T, bool>> predicate);

        IEnumerable<TResult> Select<TResult>(Expression<Func<T, TResult>> selector);

        void Create(T item);

        void Update(T item);

        void Delete(T item);
    }
}
