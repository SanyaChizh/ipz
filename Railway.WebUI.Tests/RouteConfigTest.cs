﻿using System.Web;
using System.Web.Routing;
using Moq;
using NUnit.Framework;

namespace Railway.WebUI.Tests
{
    [TestFixture]
    public class RouteConfigTest
    {
        private Mock<HttpContextBase> _mock;
        private RouteCollection _routes;

        [SetUp]
        public void SetUp()
        {
            _routes = new RouteCollection();
            RouteConfig.RegisterRoutes(_routes);
            _mock = new Mock<HttpContextBase>();
        }

        [Test]
        public void RoutesUrlTest()
        {
            _mock.Setup(m => m.Request.AppRelativeCurrentExecutionFilePath).Returns("~/");

            var routeData = _routes.GetRouteData(_mock.Object);

            Assert.IsNotNull(routeData);
            Assert.AreEqual("Route", routeData.Values["Controller"]);
            Assert.AreEqual("Routes", routeData.Values["Action"]);
        }

        [Test]
        public void FlightsUrlTest()
        {
            _mock.Setup(m => m.Request.AppRelativeCurrentExecutionFilePath).Returns("~/Flight/FlightsForRoute/1");

            var routeData = _routes.GetRouteData(_mock.Object);

            Assert.IsNotNull(routeData);
            Assert.AreEqual("Flight", routeData.Values["Controller"]);
            Assert.AreEqual("FlightsForRoute", routeData.Values["Action"]);
        }

        [Test]
        public void BasketUrlTest()
        {
            _mock.Setup(m => m.Request.AppRelativeCurrentExecutionFilePath).Returns("~/Basket/Basket");

            var routeData = _routes.GetRouteData(_mock.Object);

            Assert.IsNotNull(routeData);
            Assert.AreEqual("Basket", routeData.Values["Controller"]);
            Assert.AreEqual("Basket", routeData.Values["Action"]);
        }
    }
}
