﻿using System;
using System.Collections.Generic;
using System.Web.SessionState;
using Moq;
using NUnit.Framework;
using Railway.Domain.Core.Enums;
using Railway.Services.Dto;
using Railway.Services.Interfaces;
using Railway.WebUI.Controllers;
using Railway.WebUI.Tests.Fakes;
using Railway.WebUI.ViewModels;

namespace Railway.WebUI.Tests
{
    [TestFixture]
    public class BasketControllerTest
    {
        private const string BoughtTicketsKey = "BoughtTickets";

        private BasketController _basketController;

        private Mock<IPaymentService> _paymentService;
        private Mock<IBoughtTicketService> _boughtTicketService;
        private Mock<IClientService> _clientService;

        [SetUp]
        public void SetUp()
        {
            _basketController = new BasketController();
            _paymentService = new Mock<IPaymentService>();
            _boughtTicketService = new Mock<IBoughtTicketService>();
            _clientService = new Mock<IClientService>();

            _basketController.PaymentService = _paymentService.Object;
            _basketController.BoughtTicketService = _boughtTicketService.Object;
            _basketController.ClientService = _clientService.Object;
        }

        [Test]
        public void ShouldPayPurchases()
        {
            var model = new PayViewModel
            {
                CardNumber = "1234567812345678",
                Cvv2 = "123",
                Validity = DateTime.UtcNow.AddYears(1)
            };

            _paymentService.Setup(m => m.Pay(It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<string>()));

            var sessionItems = new SessionStateItemCollection
            {
                [BoughtTicketsKey] = new List<BuyTicketViewModel>
                {
                    new BuyTicketViewModel
                    {
                        FirstName = "FirstName",
                        Age = 0,
                        AmountOfLinens = 1,
                        AmountOfTea = 1,
                        ArrivalPointId = 1,
                        DeparturePointId = 2,
                        Email = "Email",
                        IsExistsBaggage = true,
                        LastName = "LastName",
                        PlaceId = 1,
                        StudentNumber = "StudentNumber",
                        TicketType = TicketType.Full
                    }
                }
            };
            _basketController.ControllerContext = new FakeControllerContext(_basketController, sessionItems);

            _clientService
                .Setup(m => m.GetClient(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new ClientDto(1, "firstName", "lastName", "email"));

            _boughtTicketService
                .Setup(
                    m => m.BuyTicket(
                        It.IsAny<int>(),
                        It.IsAny<int>(),
                        It.IsAny<int>(),
                        It.IsAny<int>(),
                        It.IsAny<TicketType>(),
                        It.IsAny<bool>(),
                        It.IsAny<int>(),
                        It.IsAny<int>(),
                        It.IsAny<int>(),
                        It.IsAny<string>()));

            _basketController.Pay(model);

            _paymentService.Verify(m => m.Pay(It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<string>()), Times.AtLeastOnce);

            _clientService.Verify(m => m.GetClient(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);

            _boughtTicketService
                .Verify(
                    m => m.BuyTicket(
                        It.IsAny<int>(),
                        It.IsAny<int>(),
                        It.IsAny<int>(),
                        It.IsAny<int>(),
                        It.IsAny<TicketType>(),
                        It.IsAny<bool>(),
                        It.IsAny<int>(),
                        It.IsAny<int>(),
                        It.IsAny<int>(),
                        It.IsAny<string>()),
                    Times.AtLeastOnce);
        }
    }
}
