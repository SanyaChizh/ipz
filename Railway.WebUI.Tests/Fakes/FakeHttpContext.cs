﻿using System.Web;
using System.Web.SessionState;

namespace Railway.WebUI.Tests.Fakes
{
    public class FakeHttpContext : HttpContextBase
    {
        private readonly SessionStateItemCollection _sessionItems;

        public FakeHttpContext(SessionStateItemCollection sessionItems)
        {
            _sessionItems = sessionItems;
        }

        public override HttpSessionStateBase Session => new FakeHttpSessionState(_sessionItems);
    }
}
