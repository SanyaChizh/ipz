﻿using Ninject;
using Ninject.Extensions.Interception.Infrastructure.Language;
using Railway.Domain.Interfaces;
using Railway.Infrastructure.EFDataAccess;
using Railway.Infrastructure.Services;
using Railway.Services.Interfaces;
using Railway.WebUI.Utils;

namespace Railway.WebUI
{
    public class DependenciesConfig
    {
        public static IKernel RegisterDependencies(StandardKernel kernel)
        {
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();

            kernel.Bind<IBoughtTicketService>().To<BoughtTicketService>().Intercept().With<ValidationInterceptor>();
            kernel.Bind<IClientService>().To<ClientService>().Intercept().With<ValidationInterceptor>();
            kernel.Bind<IDriverService>().To<DriverService>().Intercept().With<ValidationInterceptor>();
            kernel.Bind<IFlightService>().To<FlightService>().Intercept().With<ValidationInterceptor>();
            kernel.Bind<IPaymentService>().To<PaymentService>().Intercept().With<ValidationInterceptor>();
            kernel.Bind<IPriceService>().To<PriceService>().Intercept().With<ValidationInterceptor>();
            kernel.Bind<IRedeemTicketService>().To<RedeemTicketService>().Intercept().With<ValidationInterceptor>();
            kernel.Bind<IReservedTicketService>().To<ReservedTicketService>().Intercept().With<ValidationInterceptor>();
            kernel.Bind<IRouteService>().To<RouteService>().Intercept().With<ValidationInterceptor>();
            kernel.Bind<IShoppingCartService>().To<ShoppingCartService>().Intercept().With<ValidationInterceptor>();
            kernel.Bind<IStationDistanceService>().To<StationDistanceService>().Intercept().With<ValidationInterceptor>();
            kernel.Bind<IStationService>().To<StationService>().Intercept().With<ValidationInterceptor>();
            kernel.Bind<ITrainService>().To<TrainService>().Intercept().With<ValidationInterceptor>();
            kernel.Bind<ITrainStopService>().To<TrainStopService>().Intercept().With<ValidationInterceptor>();
            kernel.Bind<IWagonService>().To<WagonService>().Intercept().With<ValidationInterceptor>();

            kernel.Bind<ITestModelGenerator>().To<TestModelGenerator>();

            return kernel;
        }
    }
}