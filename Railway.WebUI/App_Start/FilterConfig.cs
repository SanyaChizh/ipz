﻿using System.Web.Mvc;
using Railway.WebUI.Filters;

namespace Railway.WebUI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new GlobalErrorFilter());
            filters.Add(new HandleErrorAttribute());
        }
    }
}
