﻿using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Ninject;
using Ninject.Web.Mvc;
using Railway.Infrastructure.EFDataAccess;
using Railway.WebUI.Utils;

namespace Railway.WebUI
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            var kernel = new StandardKernel();
            DependenciesConfig.RegisterDependencies(kernel);
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));

            Database.SetInitializer(new RailwayDbInitializer());
            kernel.Get<ITestModelGenerator>().GenerateTestModel();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        // public static ILogger Logger = LogManager.GetLogger("ErrorLogger");
    }
}
