﻿using System;

namespace Railway.WebUI.ViewModels
{
    public class PurchaseViewModel
    {
        public int PlaceId { get; set; }

        public string Email { get; set; }

        public string DeparturePoint { get; set; }

        public string ArrivalPoint { get; set; }

        public DateTime DeparturDate { get; set; }

        public double Price { get; set; }
    }
}