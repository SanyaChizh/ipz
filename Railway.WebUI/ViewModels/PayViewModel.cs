﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Railway.WebUI.ViewModels
{
    public class PayViewModel : IValidatableObject
    {
        [Required]
        [Display(Name = "Номер карты")]
        [RegularExpression(@"^\d{16}$", ErrorMessage = "Номер карты состоит из 16 цифр")]
        public string CardNumber { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Дата истечения срока действия карты")]
        public DateTime Validity { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [RegularExpression(@"^\d{3,4}$", ErrorMessage = "CVV2 состоит из 3 или 4 цифр")]
        public string Cvv2 { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var errors = new List<ValidationResult>();

            if (Validity <= DateTime.UtcNow)
            {
                errors.Add(new ValidationResult("Срок действия карты истек", new[] { "Validity" }));
            }

            return errors;
        }
    }
}