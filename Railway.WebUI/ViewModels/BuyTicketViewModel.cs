﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Railway.Domain.Core.Enums;

namespace Railway.WebUI.ViewModels
{
    public class BuyTicketViewModel : IValidatableObject
    {
        public int PlaceId { get; set; }

        [Required]
        [Display(Name = "Имя")]
        [MaxLength(50, ErrorMessage = "Максимальная длина имени 50")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Максимальная длина фамилии 50")]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Электронная почта")]
        public string Email { get; set; }

        public SelectList Stops { get; set; }

        [Required]
        [Display(Name = "Точка отправления")]
        public int DeparturePointId { get; set; }

        [Required]
        [Display(Name = "Точка прибытия")]
        public int ArrivalPointId { get; set; }

        [Display(Name = "Наличие багажа")]
        public bool IsExistsBaggage { get; set; }

        [Range(0, 10, ErrorMessage = "Количество чая должно быть от 0 до 10")]
        [Display(Name = "Количество чая")]
        public int AmountOfTea { get; set; }

        [Range(0, 2, ErrorMessage = "Количество комплектов постельного белья должно быть от 0 до 2")]
        [Display(Name = "Количество комплектов постельного белья")]
        public int AmountOfLinens { get; set; }

        [Display(Name = "Тип билета")]
        public TicketType TicketType { get; set; }

        [Range(0, 200)]
        [Display(Name = "Возраст (Нужен для детского билета)")]
        public int Age { get; set; }

        [Display(Name = "Студенчески номер (Серия + номер)")]
        public string StudentNumber { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var errors = new List<ValidationResult>();

            if (TicketType == TicketType.Child && (Age < 0 || Age > 14))
            {
                errors.Add(new ValidationResult("Возраст ребенка должен быть от 0 до 14 лет", new[] { "Age" }));
            }

            const string studentNumberRegex = @"\w{2}\d{8}";

            if (TicketType == TicketType.Student && (string.IsNullOrEmpty(StudentNumber) || !Regex.IsMatch(StudentNumber, studentNumberRegex)))
            {
                errors.Add(new ValidationResult("Неправильный номер студенческого билета", new[] { "StudentNumber" }));
            }

            return errors;
        }
    }
}