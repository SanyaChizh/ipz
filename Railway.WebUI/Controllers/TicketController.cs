﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ninject;
using Railway.Services.Interfaces;
using Railway.WebUI.ViewModels;

namespace Railway.WebUI.Controllers
{
    public class TicketController : Controller
    {
        private const string BoughtTicketsKey = "BoughtTickets";

        [Inject]
        public IRouteService RouteService { private get; set; }

        [HttpGet]
        public ViewResult Buy(int id)
        {
            IDictionary<string, int> stations = RouteService.GetStationsForRouteByPlace(id);

            var model = new BuyTicketViewModel
            {
                PlaceId = id,
                Stops = new SelectList(stations, "Value", "Key")
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Buy(BuyTicketViewModel model)
        {
            IDictionary<string, int> stations = RouteService.GetStationsForRouteByPlace(model.PlaceId);
            if (!ModelState.IsValid)
            {               
                model.Stops = new SelectList(stations, "Value", "Key");
                return View(model);
            }

            var ids = stations.Values.ToList();
            if (ids.IndexOf(model.ArrivalPointId) <= ids.IndexOf(model.DeparturePointId))
            {
                model.Stops = new SelectList(stations, "Value", "Key");
                ModelState.AddModelError(string.Empty, "Точка отправления должна быть перед точкой прибытия");
                return View(model);
            }

            var purchases = Session[BoughtTicketsKey] as List<BuyTicketViewModel>;
            if (purchases == null)
            {
                purchases = new List<BuyTicketViewModel>();
                Session[BoughtTicketsKey] = purchases;
            }

            purchases.Add(model);

            return RedirectToAction("Routes", new { Controller = "Route" });
        }
    }
}