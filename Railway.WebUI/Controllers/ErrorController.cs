﻿using System.Web.Mvc;

namespace Railway.WebUI.Controllers
{
    public class ErrorController : Controller
    {
        [HttpGet]
        public ViewResult ErrorPage()
        {
            return View();
        }

        [HttpGet]
        public ViewResult NotFound()
        {
            return View();
        }

        [HttpGet]
        public ViewResult Forbidden()
        {
            return View();
        }
    }
}