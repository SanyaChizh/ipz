﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ninject;
using Railway.Exceptions.Service;
using Railway.Services.Dto;
using Railway.Services.Interfaces;
using Railway.WebUI.ViewModels;

namespace Railway.WebUI.Controllers
{
    public class BasketController : Controller
    {
        private const string BoughtTicketsKey = "BoughtTickets";

        [Inject]
        public IBoughtTicketService BoughtTicketService { private get; set; }

        [Inject]
        public IClientService ClientService { private get; set; }

        [Inject]
        public IStationService StationService { private get; set; }

        [Inject]
        public IFlightService FlightService { private get; set; }

        [Inject]
        public IPaymentService PaymentService { private get; set; }

        public ViewResult Basket()
        {
            var purchases = Session[BoughtTicketsKey] as List<BuyTicketViewModel>;
            if (purchases == null)
            {
                purchases = new List<BuyTicketViewModel>();
                Session[BoughtTicketsKey] = purchases;
            }

            var model = new List<PurchaseViewModel>();

            purchases.ForEach(p =>
            {
                model.Add(new PurchaseViewModel
                {
                    PlaceId = p.PlaceId,
                    Email = p.Email,
                    DeparturePoint = StationService.Get(p.DeparturePointId).FullName,
                    ArrivalPoint = StationService.Get(p.ArrivalPointId).FullName,
                    DeparturDate = FlightService.GetFlightForPlace(p.PlaceId).DepartureDate,
                    Price = BoughtTicketService.GetTicketPrice(
                        p.PlaceId,
                        p.DeparturePointId, 
                        p.ArrivalPointId,
                        p.AmountOfTea, 
                        p.AmountOfLinens,
                        p.IsExistsBaggage,
                        p.TicketType),
                });
            });

            return View(model);
        }

        [HttpGet]
        public ActionResult DeletePurchase(int id)
        {
            var purchases = Session[BoughtTicketsKey] as List<BuyTicketViewModel>;
            if (purchases == null)
            {
                purchases = new List<BuyTicketViewModel>();
                Session[BoughtTicketsKey] = purchases;
            }
            else
            {
                var purchase = purchases.FirstOrDefault(p => p.PlaceId == id);
                purchases.Remove(purchase);
            }

            return RedirectToAction("Basket");
        }

        [HttpGet]
        public ActionResult Pay()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Pay(PayViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            PaymentService.Pay(model.CardNumber, model.Validity, model.Cvv2);
            
            var purchases = Session[BoughtTicketsKey] as List<BuyTicketViewModel>;
            if (purchases == null || purchases.Count == 0)
            {
                return RedirectToAction("ErrorPage", new { Controller = "Error" });
            }

            purchases.ForEach(BuyTicket);

            purchases.Clear();
            Session[BoughtTicketsKey] = purchases;

            return RedirectToAction("Routes", "Route");
        }

        private void BuyTicket(BuyTicketViewModel model)
        {
            int clientId;
            try
            {
                ClientDto client = ClientService.GetClient(model.FirstName, model.LastName, model.Email);
                clientId = client.Id;
            }
            catch (EntityNotFoundException)
            {
                clientId = ClientService.Create(model.FirstName, model.LastName, model.Email);
            }

            BoughtTicketService.BuyTicket(
                clientId,
                model.PlaceId,
                model.DeparturePointId,
                model.ArrivalPointId,
                model.TicketType,
                model.IsExistsBaggage,
                model.AmountOfTea,
                model.AmountOfLinens,
                model.Age,
                model.StudentNumber);
        }
    }
}