﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ninject;
using Railway.Services.Dto;
using Railway.Services.Interfaces;
using Railway.Services.Interfaces.Utils;
using Railway.WebUI.ViewModels;

namespace Railway.WebUI.Controllers
{
    public class FlightController : Controller
    {
        private const string BoughtTicketsKey = "BoughtTickets";

        [Inject]
        public IFlightService FlightService { private get; set; }

        [Inject]
        public ITrainStopService TrainStopService { private get; set; }

        [Inject]
        public IWagonService WagonService { private get; set; }

        [HttpGet]
        public ViewResult FlightsForRoute(int id)
        {
            IEnumerable<FlightDto> flights = FlightService.GetFlightsForRoute(id);

            return View(flights.ToList());
        }

        [HttpGet]
        public PartialViewResult StopsForFlight(int id)
        {
            IEnumerable<TrainStopInformation> stops = TrainStopService.GetStopsForFlight(id);

            return PartialView(stops.ToList());
        }

        [HttpGet]
        public ViewResult WagonsForFlight(int id)
        {
            IEnumerable<WagonDto> wagons = FlightService.GetWagonsForFlight(id);

            return View(wagons.ToList());
        }

        [HttpGet]
        public ViewResult PlacesForWagon(int id)
        {
            List<PlaceDto> places = WagonService.GetPlacesForWagon(id).ToList();

            var purchases = Session[BoughtTicketsKey] as List<BuyTicketViewModel>;
            if (purchases == null || purchases.Count <= 0)
            {
                return View(places);
            }

            var ids = purchases.Select(p => p.PlaceId);
            places.ForEach(p =>
            {
                if (ids.Contains(p.Id))
                {
                    p.State = Domain.Core.Models.Place.PlaceState.Busy;
                }
            });

            return View(places);
        }
    }
}