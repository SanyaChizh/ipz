﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ninject;
using Railway.Services.Dto;
using Railway.Services.Interfaces;

namespace Railway.WebUI.Controllers
{
    public class RouteController : Controller
    {
        [Inject]
        public IRouteService RouteService { private get; set; } 

        [Inject]
        public ITrainStopService TrainStopService { private get; set; }

        [HttpGet]
        public ViewResult Routes()
        {
            IEnumerable<RouteDto> routes = RouteService.GetRoutes();

            return View(routes.ToList());
        }

        [HttpGet]
        public PartialViewResult StopsForRoute(int id)
        {
            IDictionary<string, TimeSpan> stops = TrainStopService.GetStopsForRoute(id);

            return PartialView(stops as Dictionary<string, TimeSpan>);
        }
    }
}