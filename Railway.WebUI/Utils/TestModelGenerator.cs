﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using Railway.Domain.Core.Models;
using Railway.Services.Interfaces;

namespace Railway.WebUI.Utils
{
    public class TestModelGenerator : ITestModelGenerator
    {
        [Inject]
        public IBoughtTicketService BoughtTicketService { private get; set; }

        [Inject]
        public IClientService ClientService { private get; set; }

        [Inject]
        public IDriverService DriverService { private get; set; }

        [Inject]
        public IFlightService FlightService { private get; set; }

        [Inject]
        public IPriceService PriceService { private get; set; }

        [Inject]
        public IRedeemTicketService RedeemTicketService { private get; set; }

        [Inject]
        public IReservedTicketService ReservedTicketService { private get; set; }

        [Inject]
        public IRouteService RouteService { private get; set; }

        [Inject]
        public IShoppingCartService ShoppingCartService { private get; set; }

        [Inject]
        public IStationDistanceService StationDistanceService { private get; set; }

        [Inject]
        public IStationService StationService { private get; set; }

        [Inject]
        public ITrainService TrainService { private get; set; }

        [Inject]
        public ITrainStopService TrainStopService { private get; set; }

        [Inject]
        public IWagonService WagonService { private get; set; }

        public void GenerateTestModel()
        {
            GenerateStations();
            GenerateStationDistances();
            GenerateWagons();
            GenerateDrivers();
            GenerateTrains();
            GenerateTrainStops();
            GenerateRoutes();
            GenerateFlights();
            GenerateClients();
            GeneratePrices();
            GenerateShoppingCarts();
            ReserveTickets();
            BuyTickets();
        }

        private void GenerateStations()
        {
            StationService.Create("Южный вокзал", "Харьков");
            StationService.Create("Южный вокзал", "Полтава");
            StationService.Create("Южный вокзал", "Лубны");
            StationService.Create("Южный вокзал", "Киев");
        }

        private void GenerateStationDistances()
        {
            var stations = StationService.GetAllIds().ToList();

            StationDistanceService.Create(stations[0], stations[1], 143.0);
            StationDistanceService.Create(stations[1], stations[2], 142.0);
            StationDistanceService.Create(stations[2], stations[3], 199.0);
        }

        private void GenerateWagons()
        {
            var stations = StationService.GetAllIds().ToList();

            WagonService.Create(Wagon.WagonType.SittingFirstClass, stations[0], 42);
            WagonService.Create(Wagon.WagonType.SittingFirstClass, stations[0], 42);
            WagonService.Create(Wagon.WagonType.SittingSecondClass, stations[0], 43);
            WagonService.Create(Wagon.WagonType.SittingSecondClass, stations[0], 43);
        }

        private void GenerateDrivers()
        {
            var stations = StationService.GetAllIds().ToList();

            DriverService.Create("Григорий", "Панов", stations[0]);
            DriverService.Create("Анатолий", "Яковлев", stations[0]);
            DriverService.Create("Борис", "Носов", stations[0]);
        }

        private void GenerateTrains()
        {
            var stations = StationService.GetAllIds().ToList();
            var drivers = DriverService.GetAllIds().ToList();
            var wagons = WagonService.GetAllIds().ToList();

            TrainService.Create(stations[0], drivers[0], 120, new List<int> { wagons[0], wagons[2] });
            TrainService.Create(stations[0], drivers[1], 100, new List<int> { wagons[1], wagons[3] });
        }

        private void GenerateTrainStops()
        {
            var stations = StationService.GetAllIds().ToList();

            TrainStopService.Create(stations[0], new TimeSpan(0, 0, 0));
            TrainStopService.Create(stations[1], new TimeSpan(0, 20, 0));
            TrainStopService.Create(stations[2], new TimeSpan(0, 10, 0));
            TrainStopService.Create(stations[3], new TimeSpan(0, 0, 0));
        }

        private void GenerateRoutes()
        {
            var trainStops = TrainStopService.GetAllIds().ToList();

            RouteService.CreateRoute(trainStops);
        }

        private void GenerateFlights()
        {
            var trains = TrainService.GetAllIds().ToList();
            var routes = RouteService.GetAllIds().ToList();

            FlightService.Create(new DateTime(2017, 11, 2, 15, 0, 0), trains[0], routes[0], 100, 200);
            FlightService.Create(new DateTime(2017, 11, 2, 18, 20, 0), trains[1], routes[0], 100, 200);
        }

        private void GenerateClients()
        {
            ClientService.Create("Александр", "Чиж", "sasha.chizh@gmail.com");
            ClientService.Create("Влад", "Афонин", "vlad.afonin@gmail.com");
            ClientService.Create("Артем", "Кирилович", "artem.kirilovich@gmail.com");
        }

        private void GeneratePrices()
        {
            PriceService.Create("TicketReservation", 17.0f);
            PriceService.Create("PriceForOneKilometer", 0.13f);
            PriceService.Create("Tea", 5.0f);
            PriceService.Create("Linen", 10.0f);
            PriceService.Create("Baggage", 30.0f);
            PriceService.Create("StudentDiscountInPercent", 0.5f);
            PriceService.Create("ChildDiscountInPercent", 0.5f);
            PriceService.Create("PriceСoefficientForSittingSecondClass", 1.2f);
            PriceService.Create("PriceСoefficientForSittingFirstClass", 1.5f);
            PriceService.Create("PriceСoefficientForReservedSeat", 1.4f);
            PriceService.Create("PriceСoefficientForCompartment", 2.1f);
            PriceService.Create("PriceСoefficientForLux", 4.7f);
        }

        private void GenerateShoppingCarts()
        {
            ShoppingCartService.Create();
        }

        private void ReserveTickets()
        {
            var clients = ClientService.GetAllIds().ToList();
            var flights = FlightService.GetAllIds().ToList();
            var firstFlight = FlightService.Get(flights[0]);
            var route = RouteService.Get(firstFlight.RouteId);
            var train = TrainService.Get(firstFlight.TrainId);
            var firstWagon = train.Wagons.First();

            var firstPlaceId = firstWagon.Places.ToList()[0].Id;            
            var secondPlaceId = firstWagon.Places.ToList()[1].Id;
            
            var stops = route.Stops.ToList();
            var harkovStaionId = stops[0].StationId;
            var poltavaStationId = stops[1].StationId;
            var lubnyStationId = stops[2].StationId;
            var kievStationId = stops[3].StationId;

            var reservedTickets = new List<int>
            {
                ReservedTicketService.ReserveTicket(clients[0], firstPlaceId, harkovStaionId, kievStationId),
                ReservedTicketService.ReserveTicket(clients[1], secondPlaceId, poltavaStationId, lubnyStationId)
            };

            var shoppingCarts = ShoppingCartService.GetAllIds().ToList();
            ShoppingCartService.AddReservedTicketToShoppingCart(shoppingCarts[0], reservedTickets[0]);
            ShoppingCartService.AddReservedTicketToShoppingCart(shoppingCarts[0], reservedTickets[1]);
        }

        private void BuyTickets()
        {
            var clients = ClientService.GetAllIds().ToList();
            var flights = FlightService.GetAllIds().ToList();
            var reservedTickets = ReservedTicketService.GetAllIds().ToList();
            var firstFlight = FlightService.Get(flights[0]);
            var route = RouteService.Get(firstFlight.RouteId);
            var train = TrainService.Get(firstFlight.TrainId);
            var firstWagon = train.Wagons.First();
            var thirdPlaceId = firstWagon.Places.ToList()[2].Id;
            
            var stops = route.Stops.ToList();
            var harkovStaionId = stops[0].StationId;
            var poltavaStationId = stops[1].StationId;
            
            var boughtTicket = BoughtTicketService.BuyTicket(clients[2], thirdPlaceId, harkovStaionId, poltavaStationId, Domain.Core.Enums.TicketType.Student, true, 1, 1, 0, "22312431");
            var purchasedTicket = RedeemTicketService.RedeemTicket(reservedTickets[0], false, 2, 0);

            var shoppingCarts = ShoppingCartService.GetAllIds().ToList();
            ShoppingCartService.AddBoughtTicketToShoppingCart(shoppingCarts[0], boughtTicket);
            ShoppingCartService.AddReservedTicketToShoppingCart(shoppingCarts[0], purchasedTicket);
        }
    }
}
