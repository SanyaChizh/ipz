﻿using System.Reflection;
using Ninject.Extensions.Interception;
using Railway.Exceptions.Service;
using Railway.Services.Interfaces.Validators;

namespace Railway.WebUI.Utils
{
    public class ValidationInterceptor : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            ParameterInfo[] parameters = invocation.Request.Method.GetParameters();
            for (var i = 0; i < parameters.Length; i++)
            {
                foreach (ArgumentValidationAttribute attr in parameters[i].GetCustomAttributes(typeof(ArgumentValidationAttribute), true))
                {
                    object value = invocation.Request.Arguments[i];
                    string argumentName = parameters[i].Name;
                    string methodName = invocation.Request.Method.Name;
                    string className = invocation.Request.Method.DeclaringType?.FullName;
                    var details = new ArgumentExceptionDetails(value, argumentName, methodName, className);
                    attr.ValidateArgument(details);
                }
            }

            invocation.Proceed();
        }
    }
}
