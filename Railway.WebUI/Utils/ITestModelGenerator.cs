﻿namespace Railway.WebUI.Utils
{
    public interface ITestModelGenerator
    {
        void GenerateTestModel();
    }
}