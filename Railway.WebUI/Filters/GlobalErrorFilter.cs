﻿using System.Web.Mvc;
using NLog;

namespace Railway.WebUI.Filters
{
    public class GlobalErrorFilter : IExceptionFilter
    {
        private readonly ILogger _logger = LogManager.GetLogger("ErrorLogger");

        public void OnException(ExceptionContext filterContext)
        {
            _logger.Error($"Controller: {filterContext.RouteData.Values["Controller"]}," + 
                          $"action: {filterContext.RouteData.Values["action"]}, " +
                          $"exception: {filterContext.Exception}");

            filterContext.Result = new RedirectResult("/Error/ErrorPage");
            filterContext.ExceptionHandled = true;
        }
    }
}