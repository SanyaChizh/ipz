﻿; (function ($) {    
    
    // Add possible to sort table to increase and on decrease by table header with class 'sortableHeader'.
    $(".dinamicContainer").on("click", ".sortableHeader", sortRows);

    var sortOnDecreaseByNumber = true;
    var sortOnDecreaseByString = true;
    var sortOnDecreaseByDate = true;

    function sortRows(event) {
        var target = $(event.target);
        var targetPosition = target.index();
        var tBody = target.parents("tbody");
        var rows = tBody.children("tr").toArray();

        // Remove header.
        var headRow = rows.shift();
        var isNumber = target.attr("data-column-type").toLowerCase() === "number" ? true : false;
        var isString = target.attr("data-column-type").toLowerCase() === "string" ? true : false;
        var isDate = target.attr("data-column-type").toLowerCase() === "date" ? true : false;

        rows.sort(compareRows);

        if (isNumber) {
            sortOnDecreaseByNumber = sortOnDecreaseByNumber === true ? false : true;
        }
        if (isString) {
            sortOnDecreaseByString = sortOnDecreaseByString === true ? false : true;
        }
        if (isDate) {
            sortOnDecreaseByDate = sortOnDecreaseByDate === true ? false : true;
        }

        // Add header.
        rows.unshift(headRow);
        tBody.html(rows);

        function compareRows(r1, r2) {
            var firstValue = r1.children[targetPosition].innerText;
            var secondValue = r2.children[targetPosition].innerText;

            if (isNumber) {
                if (sortOnDecreaseByNumber) {
                    return compareNumbersOnDecrease(firstValue, secondValue);
                }
                else {
                    return compareNumbers(firstValue, secondValue);
                }
            }
            if (isString) {
                if (sortOnDecreaseByString) {
                    return compareStringsOnDecrease(firstValue, secondValue);
                }
                else {
                    return compareStrings(firstValue, secondValue);
                }
            }
            if (isDate) {
                if (sortOnDecreaseByDate) {
                    return compareDatesOnDecrease(firstValue, secondValue);
                }
                else {
                    return compareDates(firstValue, secondValue);
                }
            }

            return 0;
        }
    }

    function compareStrings(a, b) {
        return a > b ? 1 :
               a < b ? -1 : 0;
    }

    function compareNumbers(a, b) {
        return a - b;
    }

    function compareDates(a, b)
    {
        var first = parseDate(a);
        var second = parseDate(b);

        return first > second ? 1 :
               first < second ? -1 : 0;
    }

    function compareStringsOnDecrease(a, b) {
        return a < b ? 1 :
               a > b ? -1 : 0;
    }

    function compareNumbersOnDecrease(a, b) {
        return b - a;
    }

    function compareDatesOnDecrease(a, b) {
        var first = parseDate(a);
        var second = parseDate(b);

        return first < second ? 1 :
               first > second ? -1 : 0;
    }

    function parseDate(dateString)
    {
        var newDateString;
        if (dateString.length === 18) {
            newDateString = dateString.replace(/(\d+).(\d+).(\d+)\s(\d+):(\d+):(\d+)/, "$3-$2-$1T0$4:$5:$6.000+02:00");
        }
        if (dateString.length === 19) {
            newDateString = dateString.replace(/(\d+).(\d+).(\d+)\s(\d+):(\d+):(\d+)/, "$3-$2-$1T$4:$5:$6.000+02:00");
        }
        if (dateString.length === 10) {
            newDateString = dateString.replace(/(\d+).(\d+).(\d+)/, "$3-$2-$1");
        }

        var dateInMs = Date.parse(newDateString);
        var date = new Date();
        date.setTime(dateInMs);
        return date;
    }

})(jQuery);