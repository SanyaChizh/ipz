﻿; (function ($) {

    $(".wagonType").each(function () {
        var target = $(this);
        var text = target.text();

        switch (text) {
            case "Lux":
                target.text("Люкс");
                break;

            case "Compartment":
                target.text("Купе");
                break;

            case "SittingFirstClass":
                target.text("Сидящий первого класса");
                break;

            case "SittingSecondClass":
                target.text("Сидящий второго класса");
                break;

            case "ReservedSeat":
                target.text("Плацкарт");
                break;
        }
    });

})(jQuery);