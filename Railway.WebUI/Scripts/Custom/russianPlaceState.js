﻿; (function ($) {

    $(".placeState").each(function () {
        var target = $(this);
        var text = target.text();

        switch (text) {
            case "Free":
                target.text("Свобдное");
                break;

            case "Busy":
                target.text("Занятое");
                break;

            case "Reserved":
                target.text("Зарезервированное");
                break;
        }
    });

})(jQuery);