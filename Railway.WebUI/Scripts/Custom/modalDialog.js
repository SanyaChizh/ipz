﻿; (function ($) {

    // Load and show bootstrap modal dialog.
    $.ajaxSetup({ cache: false });
    $(".dinamicContainer").on("click", ".compItem", function (e) {

        e.preventDefault();
        $.get(this.href, function (data) {
            $("#modalDialogContent").html(data);
            var modDialog = $("#modDialog");
            modDialog.modal("show");
        });
    });

})(jQuery);