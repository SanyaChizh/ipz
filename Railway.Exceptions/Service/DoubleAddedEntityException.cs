﻿using System;

namespace Railway.Exceptions.Service
{
    public class DoubleAddedEntityException : Exception
    {
        public DoubleAddedEntityException(string message) : base(message)
        {
        }

        public DoubleAddedEntityException()
        {
        }
    }
}
