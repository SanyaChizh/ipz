﻿namespace Railway.Exceptions.Service
{
    public class ArgumentExceptionDetails
    {
        public ArgumentExceptionDetails(
            object value,
            string argumentName,
            string methodName,
            string className)
        {
            Value = value;
            ArgumentName = argumentName;
            MethodName = methodName;
            ClassName = className;
        }

        public object Value { get; private set; }

        public string ArgumentName { get; private set; }

        public string MethodName { get; private set; }

        public string ClassName { get; private set; }

        public string Message { get; set; }
    }
}
