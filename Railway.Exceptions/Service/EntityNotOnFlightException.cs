﻿using System;

namespace Railway.Exceptions.Service
{
    public class EntityNotOnFlightException : Exception
    {
        public EntityNotOnFlightException(Type type) : base($"Entity: {type} not on flight")
        {
        }
    }
}
