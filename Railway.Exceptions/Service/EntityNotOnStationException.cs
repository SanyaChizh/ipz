﻿using System;

namespace Railway.Exceptions.Service
{
    public class EntityNotOnStationException : Exception
    {
        public EntityNotOnStationException(Type type) : base($"Entity {type} must be on station")
        {
        }
    }
}
