﻿using System;

namespace Railway.Exceptions.Service
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(Type type)
        {
            Type = type;
        }

        public EntityNotFoundException(Type type, string message) : base(message)
        {
            Type = type;
        }

        public Type Type { get; private set; }
    }
}
