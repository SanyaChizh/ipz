﻿using System;

namespace Railway.Exceptions.Service
{
    public class InvalidModelStateForOperationException : Exception
    {
        public InvalidModelStateForOperationException(string message) : base(message)
        {
        }
    }
}
