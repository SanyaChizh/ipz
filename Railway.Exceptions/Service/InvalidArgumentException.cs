﻿using System;

namespace Railway.Exceptions.Service
{
    public class InvalidArgumentException : Exception
    {
        public InvalidArgumentException(ArgumentExceptionDetails details)
        {
            Details = details;
        }

        public InvalidArgumentException()
        {
        }

        public ArgumentExceptionDetails Details { get; set; }
    }
}
