﻿using System;

namespace Railway.Exceptions.Service
{
    public class FailedUserOperationException : Exception
    {
        public FailedUserOperationException(string message) : base(message)
        {
        }

        public FailedUserOperationException()
        {
        }
    }
}
