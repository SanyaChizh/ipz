﻿using System;

namespace Railway.Exceptions.Model
{
    public class InvalidArgumentStateException : Exception
    {
        public InvalidArgumentStateException(string message) : base(message)
        {
        }
    }
}
