﻿using System;

namespace Railway.Exceptions.Model
{
    public class ModelCreatingException : Exception
    {
        public ModelCreatingException(string message) : base(message)
        {
        }

        public ModelCreatingException()
        {
        }
    }
}
