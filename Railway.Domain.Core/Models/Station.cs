﻿using System.Collections.Generic;

namespace Railway.Domain.Core.Models
{
    public class Station
    {
        public Station(string name, string city)
        {
            Trains = new List<Train>();
            Wagons = new List<Wagon>();
            Drivers = new List<Driver>();
            Name = name;
            City = city;
        }

        public Station()
        {
            Trains = new List<Train>();
            Wagons = new List<Wagon>();
            Drivers = new List<Driver>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string City { get; set; }

        public string FullName => $"{City} ({Name})";

        public virtual ICollection<Train> Trains { get; set; }

        public virtual ICollection<Wagon> Wagons { get; set; }

        public virtual ICollection<Driver> Drivers { get; set; }
    }
}
