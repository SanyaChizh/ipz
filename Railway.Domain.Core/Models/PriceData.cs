﻿namespace Railway.Domain.Core.Models
{
    public class PriceData 
    {
        public PriceData(string name, float value)
        {
            Name = name;
            Value = value;
        }

        public PriceData()
        {
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public float Value { get; set; }
    }
}
