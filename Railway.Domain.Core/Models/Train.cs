﻿using System.Collections.Generic;

namespace Railway.Domain.Core.Models
{
    public class Train 
    {
        public Train(Station station, Flight flight, Driver driver, int speed)
        {
            Wagons = new List<Wagon>();
            Station = station;
            Flight = flight;
            Driver = driver;
            Speed = speed;
        }

        public Train()
        {
            Wagons = new List<Wagon>();
        }

        public int Id { get; set; }

        public virtual Station Station { get; set; }

        public virtual Flight Flight { get; set; }

        public virtual Driver Driver { get; set; }

        public virtual ICollection<Wagon> Wagons { get; set; }

        public int Speed { get; set; }

        public bool IsTrainOnFlight()
        {
            return Flight != null;
        }
    }
}
