﻿namespace Railway.Domain.Core.Models
{
    public class Place 
    {
        public Place(PlaceState state, Wagon wagon)
        {
            State = state;
            Wagon = wagon;
        }

        public Place()
        {
        }

        public enum PlaceState { Free, Busy, Reserved }

        public int Id { get; set; }

        public PlaceState State { get; set; }

        public virtual Wagon Wagon { get; set; }
    }
}
