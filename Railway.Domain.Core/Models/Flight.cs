﻿using System;

namespace Railway.Domain.Core.Models
{
    public class Flight 
    {
        public Flight(
            FlightState state,
            DateTime departureDate,
            DateTime arrivalDate,
            Train train,
            Route route,
            int amountOfLinens,
            int amountOfTea)
        {
            State = state;
            DepartureDate = departureDate;
            ArrivalDate = arrivalDate;
            Train = train;
            Route = route;
            AmountOfTea = amountOfTea;
            AmountOfLinens = AmountOfLinens;
        }

        public Flight()
        {
        }

        public enum FlightState { Ready, InTheWay, Late, Completed, Canceled }

        public int Id { get; set; }

        public FlightState State { get; set; }

        public DateTime DepartureDate { get; set; }

        public DateTime ArrivalDate { get; set; }

        public virtual Train Train { get; set; }

        public virtual Route Route { get; set; }

        public int AmountOfLinens { get; set; }

        public int AmountOfTea { get; set; }
    }
}
