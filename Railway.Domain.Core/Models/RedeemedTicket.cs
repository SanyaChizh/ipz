﻿using System;

namespace Railway.Domain.Core.Models
{
    public class RedeemedTicket : ReservedTicket
    {
        public RedeemedTicket(
            Client client,
            Place place,
            string departurePoint,
            string arrivalPoint,
            DateTime reservationDate,
            string fiscalReceiptNumber,
            DateTime redeemDate,
            TicketAttributes attributes) : base(client, place, departurePoint, arrivalPoint, reservationDate)
        {
            FiscalReceiptNumber = fiscalReceiptNumber;
            PurchaseDate = redeemDate;
            Attributes = attributes;
        }

        public RedeemedTicket()
        {
        }

        public string FiscalReceiptNumber { get; private set; }

        public DateTime PurchaseDate { get; private set; }

        public TicketAttributes Attributes { get; private set; }
    }
}
