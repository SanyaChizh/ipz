﻿using Railway.Exceptions.Model;

namespace Railway.Domain.Core.Models
{
    public abstract class Ticket
    {
        protected Ticket(Client client, Place place, string departurePoint, string arrivalPoint)
        {
            if (place.Wagon.IsWagonOnFlight() == false)
            {
                throw new InvalidArgumentStateException("Wagon of selected place doesn't assigned on flight");
            }

            Client = client;
            Place = place;
            DeparturePoint = departurePoint;
            ArrivalPoint = arrivalPoint;
        }

        protected Ticket()
        {
        }

        public int Id { get; private set; }

        public virtual Client Client { get; private set; }

        public virtual Place Place { get; private set; }

        public string DeparturePoint { get; private set; }

        public string ArrivalPoint { get; private set; }
    }
}
