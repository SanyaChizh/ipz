﻿using System;

namespace Railway.Domain.Core.Models
{
    public class BoughtTicket : Ticket
    {
        public BoughtTicket(
            Client client,
            Place place,
            string departurePoint,
            string arrivalPoint,
            string fiscalReceiptNumber,
            DateTime purchaseDate,
            TicketAttributes attributes) : base(client, place, departurePoint, arrivalPoint)
        {
            FiscalReceiptNumber = fiscalReceiptNumber;
            PurchaseDate = purchaseDate;
            Attributes = attributes;
        }

        public BoughtTicket()
        {
        }

        public string FiscalReceiptNumber { get; private set; }

        public DateTime PurchaseDate { get; private set; }

        public TicketAttributes Attributes { get; private set; }
    }
}
