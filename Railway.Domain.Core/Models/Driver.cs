﻿namespace Railway.Domain.Core.Models
{
    public class Driver
    {
        public Driver(string firstName, string lastName, Train train, Station station)
        {
            FirstName = firstName;
            LastName = lastName;
            Train = train;
            Station = station;
        }

        public Driver()
        {
        }

        public int Id { get; private set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public virtual Train Train { get; set; }

        public virtual Station Station { get; set; }
    }
}
