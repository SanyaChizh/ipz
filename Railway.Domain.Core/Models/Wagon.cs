﻿using System.Collections.Generic;

namespace Railway.Domain.Core.Models
{
    public class Wagon 
    {
        public Wagon(WagonType type, Train train, Station station, int amountPfPlaces)
        {
            Places = new List<Place>();
            Type = type;
            Train = train;
            Station = station;
            AmountOfPlaces = amountPfPlaces;
        }

        public Wagon()
        {
            Places = new List<Place>();
        }

        public enum WagonType
        {
            Lux,
            Compartment,
            SittingFirstClass,
            SittingSecondClass,
            ReservedSeat
        }

        public int Id { get; set; }

        public WagonType Type { get; set; }

        public virtual Train Train { get; set; }

        public virtual Station Station { get; set; }

        public virtual ICollection<Place> Places { get; set; }

        public int AmountOfPlaces { get; set; }

        public bool IsWagonOnFlight()
        {
            if (Train == null)
            {
                return false;
            }

            return Train.IsTrainOnFlight();
        }
    }
}
