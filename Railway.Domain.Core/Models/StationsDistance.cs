﻿using System.Collections.Generic;

namespace Railway.Domain.Core.Models
{
    public class StationsDistance
    {
        public StationsDistance(Station firstStation, Station secondStation, double distance)
        {
            Stations = new List<Station> { firstStation, secondStation };
            Distance = distance;
        }

        public StationsDistance()
        {
            Stations = new List<Station>();
        }

        public int Id { get; private set; }

        public virtual ICollection<Station> Stations { get; private set; }

        public double Distance { get; private set; }
    }
}
