﻿using System.Collections.Generic;
using System.Linq;

namespace Railway.Domain.Core.Models
{
    public class Route 
    {
        public Route()
        {
            Stops = new List<TrainStop>();
            Flights = new List<Flight>();
        }

        public int Id { get; set; }

        public virtual ICollection<TrainStop> Stops { get; set; }

        public virtual ICollection<Flight> Flights { get; set; }

        public string DeparturePoint => Stops.First().Station.FullName;

        public string ArrivalPoint => Stops.Last().Station.FullName;
    }
}
