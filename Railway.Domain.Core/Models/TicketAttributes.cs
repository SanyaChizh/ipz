﻿using Railway.Domain.Core.Enums;
using Railway.Exceptions.Model;

namespace Railway.Domain.Core.Models
{
    public class TicketAttributes
    {
        public TicketAttributes(
            TicketType type,
            string studentNumber,
            bool isExistsBaggage,
            int age,
            int amountOfTea,
            int amountOfLinens,
            float price)
        {
            CheckTicketTypeValidity(type, studentNumber, age);
            Type = type;
            StudentNumber = studentNumber;
            IsExistsBaggage = isExistsBaggage;
            Age = age;
            AmountOfTea = amountOfTea;
            AmountOfLinens = AmountOfLinens;
            Price = price;
        }

        public TicketAttributes()
        {
        }

        public TicketType Type { get; private set; }

        public string StudentNumber { get; private set; }

        public bool IsExistsBaggage { get; private set; }

        public int Age { get; private set; }

        public int AmountOfTea { get; private set; }

        public int AmountOfLinens { get; private set; }

        public float Price { get; private set; }

        private void CheckTicketTypeValidity(TicketType type, string studentNumber, int age)
        {
            if (type == TicketType.Child && (age < 0 || age > 14))
            {
                throw new ModelCreatingException("Invalid age for child ticket");
            }

            if (type == TicketType.Student && string.IsNullOrWhiteSpace(studentNumber))
            {
                throw new ModelCreatingException("Invalid student number for student ticket");
            }
        }
    }
}
