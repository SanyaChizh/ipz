﻿using System;

namespace Railway.Domain.Core.Models
{
    public class ReservedTicket : Ticket
    {
        public ReservedTicket(Client client, Place place, string departurePoint, string arrivalPoint, DateTime reservationDate) 
            : base(client, place, departurePoint, arrivalPoint)
        {
            ReservetionDate = reservationDate;
        }

        public ReservedTicket()
        {           
        }

        public DateTime ReservetionDate { get; private set; }
    }
}
