﻿using System.Collections.Generic;

namespace Railway.Domain.Core.Models
{
    public class ShoppingCart
    {
        public ShoppingCart()
        {
            Tickets = new List<Ticket>();
        }

        public int Id { get; private set; }

        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
