﻿using System;

namespace Railway.Domain.Core.Models
{
    public class TrainStop 
    {
        public TrainStop(Station station, TimeSpan time)
        {
            Station = station;
            Time = time;
        }

        public TrainStop()
        {
        }

        public int Id { get; private set; }

        public virtual Station Station { get; private set; }

        public TimeSpan Time { get; set; }
    }
}
